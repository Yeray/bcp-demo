<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('REVISR_GIT_PATH', ''); // Added by Revisr
define('REVISR_WORK_TREE', '/var/www/vhosts/pitchandmatch.net/bcpdemo.pitchandmatch.net/wordpress/'); // Added by Revisr
define('DB_NAME', 'pnm_21_06');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', '192.168.1.123');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'ZbSSgozNks!3!jiBguz)MAalNTnLdnC90VIuC!9DC)RjWK13VGkMaJn9CSXVidn@');
define('SECURE_AUTH_KEY',  'xmjIyAguzLdjDFRLvZq9o6d8S(xquspP*&kIDv6BQEN^JQ5dJ&vOOR%RIY8g(ZP5');
define('LOGGED_IN_KEY',    'sX%9%e(hHWZ)bJn^NenvZg6OI5W)&p6J8kSz@45&NpTnCZO@@j#sbvSe8wC8dPYA');
define('NONCE_KEY',        'VjMcpsKQQsK4mUxr8QdN%4WQzJKgCyRhLWF)VNw3fA8CrT9ld3KHk9yb4bdJtmx2');
define('AUTH_SALT',        'FeCmWS)HoZkClp!hp#jqerysE*drP6nE(sl4FFiWbG7GrcxkqIsOIkz@8lzJLyLK');
define('SECURE_AUTH_SALT', 'Wn4)Of*#ds!p0VVyNTAo7vFM%7@iLMLf10d6&w4Wtksy%@x0YeC5ITohDKN)0D)U');
define('LOGGED_IN_SALT',   ')!ctIh9L8F#M41SZL9uDTLp%kyNcR4O)Y8mQyJiwuvPr((#3qEN!SgV@dP7i%rkl');
define('NONCE_SALT',       'HUM!5^ozWP#@PCw@CKBFw&cWBGWn&39tO*aDC^dt9S3SWjUASabFT9hGuzDTMUvP');
/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = '0a8eLi_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

define( 'WP_ALLOW_MULTISITE', true );

define ('FS_METHOD', 'direct');
