
---Downloaded files---
The import files for: Thrive Intranet & Extranet Demo were successfully downloaded!
Initial max execution time = 3000
Files info:
Site URL = http://bcpdemo.pitchandmatch.net/wordpress
Data file = /var/www/vhosts/pitchandmatch.net/bcpdemo.pitchandmatch.net/wordpress/wp-content/uploads/2018/05/demo-content-import-file_2018-05-09__12-02-22.xml
Widget file = /var/www/vhosts/pitchandmatch.net/bcpdemo.pitchandmatch.net/wordpress/wp-content/uploads/2018/05/demo-widgets-import-file_2018-05-09__12-02-22.json
Customizer file = not defined!
Redux files:
not defined!

---pt-ocdi/before_widgets_import---


---Importing widgets---
Sidebar : 

Gears: Social Media Widget - Social Media - Imported
Thrive: Featured Group - Featured Group - Imported
Gears: Recent Posts - Recent Posts - Imported
Events List - Upcoming Events - Imported
wpknb_widget - Site Reference - Site does not support widget

BP Sidebar : 

Thrive: Featured Member - Featured Member - Imported
Thrive: Featured Group - Featured Group - Imported
bp_core_friends_widget - No Title - Site does not support widget
bp_groups_widget - Groups - Site does not support widget
(BuddyPress) Members - Popular Members - Imported

Sidenav Sidebar : 

(TaskBreaker) My Recent Task - My Recent Tasks - Imported
(BuddyPress) Recently Active Members - Recently Active Members - Imported
(BuddyPress) Members - Members - Imported
Text - Info - Imported

Archives : 

Gears: Social Media Widget - Social Media - Imported
Thrive: Featured Member - Writer of the Month - Imported
Gears: Recent Posts - Recent Posts - Imported
Events List - Upcoming Events - Imported
wpknb_widget - Site Reference - Site does not support widget

Dashboard : 

Events List - Upcoming Events - Imported
(TaskBreaker) My Recent Task - My Recent Tasks - Imported
Thrive: Birthday Widget - Upcoming Birthdays - Imported
Thrive: Featured Member - Featured Member - Imported
Gears: Recent Posts - Recent Posts - Imported
Thrive: Featured Group - Featured Group - Imported
(BuddyPress) Members - Staff - Imported
bp_groups_widget - Groups - Site does not support widget
Polls - Polls - Imported

Footer : 

Text - About - Imported
bbp_search_widget - Search Forums - Site does not support widget
(BuddyPress) Recently Active Members - Recently Active Members - Imported
wpknb_widget - Site Reference - Site does not support widget

Projects : 

(TaskBreaker) My Recent Task - My Recent Tasks - Imported
Thrive: Birthday Widget - Birthdays - Imported
Gears: Recent Posts - Recent Posts - Imported



---pt-ocdi/after_import---

