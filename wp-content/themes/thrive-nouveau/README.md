<h2>Thrive 3.0 "Nouveau"</h2>

You're working on your new WordPress website and planning to have a community where people can talk to each other, share ideas, and foster a simple community in your chosen niche. 

What you'll do? First, you build a theme, then you code the plugins, release the site and manage it.  That seems like much work! What this piece of little Software/Theme does is remove those two things I've mentioned, (about building and coding) so you could focus on things that make you more money like managing your business and marketing.

<h3>Introducing Thrive</h3>

Thrive has been designed as the ultimate foundation for your collaborative online community, bringing together social networking, messaging, forums, events, project and task management, file sharing, e-commerce and even e-learning.

This innovative theme can be tailored to your needs so that your members can easily share messages, files, events, tickets and forms, edit and collaborate on documents, and manage files and projects. Thrive also offers a private site option, with a secure login for members.

<h3>Minimum System Requirements</h3>
<ol>
	<li>PHP Version 5.6+ </li>
	<li>MySQL 5.6+ </li>
	<li>BuddyPress 3.0+</li>
	<li>WordPress 4.5+</li>
</ol>

<h3>Recommended Server Requirements</h3>
<ol>
	<li>Atleast 1 GB RAM</li>
	<li>Solid State Drive</li>
	<li>Apache2/Nginx </li>
	<li>Amazon S3 for CDN</li>
	<li>VPS/Dedicated Hosting (Stay away from shared hosting)</li>
</ol>

<h3>Recommended Server Set-up (php.ini)</h3>
<ol>
	<li>memory_limit = 1024M</li>
	<li>max_execution_time = 60 (Make the value zero when running import)</li>
</ol>

<h3>Development Set-up</h3>

<ol>
	<li>Use [npm install grunt --save-dev]</li>
	<li>Use [npm install grunt-sass --save-dev]</li>
	<li>Use [npm install grunt-contrib-watch --save-dev]</li>
	<li>Use 'grunt' command to start a project</li>
</ol>
<p>
	<strong>
		Shorthand:
	</strong>
<p>
npm install grunt --save-dev; npm install grunt-sass --save-dev; npm install grunt-contrib-watch --save-dev 
</p>
<a href="https://dunhakdis.com/thrive-intranet-community-theme/">Documentation</a> | 
<a href="https://themeforest.net/user/dunhakdis?ref=dunhakdis&utm_src=thrive_readme">Author</a>

