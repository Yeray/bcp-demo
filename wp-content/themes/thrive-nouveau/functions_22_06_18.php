<?php
/**
 * Dunhakdis functions and definitions
 *
 * @package thrive
 */

define( 'THRIVE_THEME_VERSION', '3.0.3' );

if ( ! function_exists( 'thrive_init' ) ):

    function thrive_init() {

        // Add styling to the default editor
		add_editor_style( get_template_directory_uri() . '/css/editor-style.css');
    }

    add_action('init', 'thrive_init');

endif;

if ( ! function_exists( 'thrive_setup' ) ) :
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function thrive_setup() {
        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on thrive, use a find and replace
         * to change 'thrive-nouveau' to the name of your theme in all the template files
         */
		load_theme_textdomain( 'thrive-nouveau', get_template_directory() . '/languages' );

        // Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

        // Use nouveau
		add_theme_support( 'buddypress-use-nouveau' );

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
		add_theme_support( 'title-tag' );

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
         */
		add_theme_support( 'post-thumbnails' );

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
		add_theme_support( 'html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
		) );

        /**
         * Add Image Size
         */
         add_image_size( 'thrive-thumbnail', 590, 250, true );

        /*
         * Enable support for Post Formats.
         * See http://codex.wordpress.org/Post_Formats
         */
        // add_theme_support( 'post-formats', array() );

        // add_theme_support( 'custom-header', $args = array() );
        // add_theme_support( 'custom-background', $args = array() );

    }

	if ( function_exists('bp_is_active') ) {
		if ( bp_is_active('activity') ) {
			define( 'BP_DEFAULT_COMPONENT', 'activity' );
        }
    }

    /**
     * Support WooCommerce
     */
	add_action( 'after_setup_theme', 'woocommerce_support' );

	function woocommerce_support()
	{
        /**
         * Support WooCommerce.
         */
	    add_theme_support( 'woocommerce' );
        /**
         * Add support for woocommerce zoom
         */
	    add_theme_support( 'wc-product-gallery-zoom' );
        /**
         * Add support for woocommerce lightbox
         */
	    add_theme_support( 'wc-product-gallery-lightbox' );
        /**
         * Add support for woocommerce slider
         */
	    add_theme_support( 'wc-product-gallery-slider' );
    }

endif; // End function thrive_setup.

add_action( 'after_setup_theme', 'thrive_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function thrive_content_width() {

	$GLOBALS['content_width'] = apply_filters( 'thrive_content_width', 750 );

}

add_action( 'after_setup_theme', 'thrive_content_width', 0 );

if ( ! isset( $content_width ) ) {
    $content_width = 850;
}

/**
 * Register Google Fonts
 *
 * @return  string The url of the google font.
 */
function thrive_google_fonts_url() {

    $font_url = '';

    $font_code = apply_filters( 'thrive_google_font', 'Roboto:300,400,500,700,400italic,500italic,700italic,300italic|Noto+Serif:400,400i,700' );
    /*
      Translators: If there are characters in your language that are not supported
      by chosen font(s), translate this to 'off'. Do not translate into your own language.
     */
    if ( 'off' !== _x( 'on', 'Google font: on or off', 'thrive-nouveau' ) ) {

        $font_url = add_query_arg( 'family', $font_code, "//fonts.googleapis.com/css" );

    }

    return $font_url;
}

/**
 * Check if user enable RTL option inside WordPress Customizer
 * @return boolean True if user enabled RTL. Otherwise, false.
 */
function thrive_is_rtl() {

    // @todo: Create rtl option inside customizer
    // $rtl_option = get_theme_mod('rtl_option');
    $rtl_option = get_theme_mod('thrive_layouts_rtl', 'no');

	if ( "yes" === $rtl_option ) {

        return true;

    }

    return false;

}

/**
 * Enqueue scripts and styles.
 */
function thrive_scripts() {

    //--- START CSS STYLESHEETS ---//
	wp_enqueue_style( 'thrive-google-font', thrive_google_fonts_url(), array(), THRIVE_THEME_VERSION );
	wp_enqueue_style( 'thrive-bootstrap', get_template_directory_uri() . '/css/bootstrap.css', array('thrive-google-font'), THRIVE_THEME_VERSION );

    // Add theme support for LearnDash LMS.
	if ( in_array( 'sfwd-lms/sfwd_lms.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {

		wp_enqueue_style('thrive-learndash-style', get_template_directory_uri() . '/css/learndash.css', array(), THRIVE_THEME_VERSION );
        wp_dequeue_style('wpProQuiz_front_style');

    }

	wp_enqueue_style( 'thrive-style', get_stylesheet_uri(), array('thrive-bootstrap'), THRIVE_THEME_VERSION );

    // RTL
	if ( thrive_is_rtl() ) {
		wp_enqueue_style( 'bootstrap-rtl', '//cdn.rawgit.com/morteza/bootstrap-rtl/master/dist/css/bootstrap-rtl.min.css', 
			array('thrive-bootstrap-rtl'), THRIVE_THEME_VERSION );
		wp_enqueue_style( 'thrive-rtl', get_template_directory_uri() . '/rtl.css', array('thrive-style'), THRIVE_THEME_VERSION );
    }

    //--- END CSS STYLESHEETS ---//


	wp_enqueue_script( 'thrive-navigation', get_template_directory_uri() . '/js/navigation.js', array(), THRIVE_THEME_VERSION, true );
	wp_enqueue_script( 'thrive-jquery-plugins', get_template_directory_uri() . '/js/jquery-plugins.js', array( 'jquery' ), THRIVE_THEME_VERSION, true );

    // Drag and drop dashboard page.
    $is_dashboard = is_page_template('page-templates/dashboard.php');
    // Disable the drag and drop functionality in the meantime
    $is_dashboard = false;
	if ( $is_dashboard ) {
		wp_enqueue_script( 'thrive-packery', get_template_directory_uri() . '/js/packery.min.js', array(), THRIVE_THEME_VERSION, true );
		wp_enqueue_script( 'thrive-dashboard-drag-drop', get_template_directory_uri() . '/js/thrive-dashboard.js', array('jquery') );
    	wp_localize_script( 'thrive-dashboard-drag-drop', 'thrive_dashboard_widgets',  array(
    		'ajax_url' => admin_url( 'admin-ajax.php' ),
    		'user_widgets_position' => get_user_meta( get_current_user_id(), 'thrive_user_dashboard_widget_position_packery' ),

        ));
    }
    // Drag and drop dashboard page end.

	wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array(), THRIVE_THEME_VERSION, true );

	wp_enqueue_script( 'thrive-script', get_template_directory_uri() . '/js/thrive.js', array(), THRIVE_THEME_VERSION, true );

	wp_enqueue_script( 'thrive-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), THRIVE_THEME_VERSION, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
    }

    return;

}

add_action( 'wp_enqueue_scripts', 'thrive_scripts' );
/**
 * Disable BuddyPress Cover Photo
 */
add_filter( 'bp_is_profile_cover_image_active', '__return_false' );
add_filter( 'bp_is_groups_cover_image_active', '__return_false' );

/**
 * Require the menu
 */
require get_template_directory() . '/thrive/thrive.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Load WooCommerce compatability file.
 */
require get_template_directory(). '/inc/woocommerce.php';

/**
 * Option Tree Settings.
 */
if ( class_exists( 'Gears' ) ) {

	add_filter( 'ot_theme_options_page_title', 'thrive_options_page_title');

	function thrive_options_page_title() { return __('Social Connect ', 'thrive-nouveau'); }

	add_filter( 'ot_theme_options_menu_title', 'thrive_options_menu_title');

	function thrive_options_menu_title() { return __('Social Connect ', 'thrive-nouveau'); }

	add_filter( 'ot_header_version_text', 'thrive_ot_header_version_text');

	function thrive_ot_header_version_text(){ return __('Thrive Social Connect ', 'thrive-nouveau') . THRIVE_THEME_VERSION; };

	add_filter( 'gears_widget_recent_posts_is_enabled', '__return_true' );
    add_filter( 'gears_widget_social_media_link_is_enabled', '__return_true' );
    add_filter( 'gears_counters_enabled', '__return_true' );

}

/**
 * WP Login CSS.
 */
require get_template_directory() . '/inc/wp-login-css.php';

/**
 * BuddyPress theme functions.
 */
require get_template_directory() . '/inc/bp-functions/thrive-bp-messages.php';
require get_template_directory() . '/inc/bp-functions/thrive-bp-notifications.php';


/**
 * Include Kirky
 */
require_once get_template_directory() . '/customizer/include-kirky.php';

/**
 * Include Thrive Class
 */
require_once get_template_directory() . '/customizer/class-thrive-kirky.php';

/**
 * Include Customizer Bootstrap
 */
require_once get_template_directory() . '/customizer/customizer.php';

/**
 * Include the theme welcome page.
 */
require_once get_template_directory() . '/inc/theme-welcome.php';

/**
 * Include the nav walker for bootstrap
 */
require_once get_template_directory() . '/inc/bootstrap-nav-walker.php';

/**
 * Include the installer
 */
require_once get_template_directory() . '/installer/bootstrap.php';

/**
 * Include the config file
 */
require_once get_template_directory() . '/installer/import-config.php';



function add_pm_event_dropdown() {
    if (class_exists('Add_API_key')) {
        global $wpdb;
        $pm = new Add_API_key();
        $events = $pm->getEvents();
        $table = $wpdb->prefix . "postmeta";
        $allready_linked_events = $wpdb->get_results("select meta_value from $table where meta_key = 'linked_event' and meta_value is not null and meta_value != '' ");
        $l_events = [];
        foreach ($allready_linked_events as $key => $value) {
            $l_events[] = $value->meta_value;
        }
        add_meta_box('pm_event', 'PITCH AND MATCH EVENTS', 'pm_event_dropdown_callback', 'tribe_events', 'advanced', 'default', array('events' => $events, 'l_events' => $l_events, 'pm' => $pm)
        );
    }
}

function pm_event_dropdown_callback($post_data, $metabox) {
    wp_nonce_field(basename(FILE), "meta-box-nonce");
    $events = $metabox['args']['events'];
    $l_events = $metabox['args']['l_events'];
    $pm = $metabox['args']['pm'];
    $wp_to_pm_linked_event = get_post_meta($post_data->ID, 'linked_event', true);
    ?>
    <div>
        <label for="pm_events">Events List</label>
        <select name="pm_events" class="tribe-dropdown">
            <option value="">Please select event</option>
    <?php
    if (!isset($events->error)) {
        foreach ($events as $key => $event) {
            if (!in_array($event->id, $l_events) || $event->id == $wp_to_pm_linked_event) {
                ?>
                        <option value="<?php echo $event->id; ?>" <?php echo ($event->id == $wp_to_pm_linked_event) ? "selected" : "" ?>><?php echo $event->name; ?></option>
                        <?php
                    }
                }
            }
            ?>
        </select>
            <?php
            if (isset($pm->api_key) && $pm->api_key == "" && !isset($events->error)) {
                echo "<span style='color:red;padding-left: 30px;'>PitchandMatch API key is not set</span>";
            } elseif (isset($events->error)) {
                echo "<span style='color:red;padding-left: 30px;'>PitchandMatch API key is not valid</span>";
            }
            ?>
        <br/>
    </div>
        <?php
    }

    /**
     * Events dropwdown from PitchandMatch API.
     */
    add_action("add_meta_boxes", "add_pm_event_dropdown");


    function add_linked_events_meta($post_id) {
        $wp_pm_prev_linked_event = get_post_meta($post_id, 'linked_event', true);
        update_post_meta($post_id, 'linked_event', $_POST['pm_events']);

        if (isset($_POST['pm_events']) && !empty($_POST['pm_events'])) {
            $re = new Add_API_key();

            if (!empty($wp_pm_prev_linked_event) && $wp_pm_prev_linked_event != $_POST['pm_events']) {
                $webhook_id = get_post_meta($post_id, 'linked_webhook_id', true);
                if (!empty($webhook_id)) {
                    $res = $re->deleteWebhook($webhook_id);
//            if ($res == 200) 
                }
                update_post_meta($post_id, 'linked_webhook_id', NULL);
            }
            $linked_webhook = get_post_meta($post_id, 'linked_webhook_id', true);
            if (empty($linked_webhook)) {
                $res = $re->createWebhook($_POST['pm_events']);
                if (isset($res->webhooks)) {
                    $webhook_id = $res->webhooks->id;
                    update_post_meta($post_id, 'linked_webhook_id', $webhook_id);
                }
            }
        }
    }

    /**
     * Link the WP events to PM events. 
     */
    add_action('save_post_tribe_events', 'add_linked_events_meta', 10, 4);



    /*
     * API class for receiving attendee data from PM
     *  */
    require get_template_directory() . '/class-pm-rest-server.php';
    $wp_webhook_api = new PM_Rest_Server();
    $wp_webhook_api->hook_rest_server();


    function members_event_dropdown() {
        ?>

    <div id="dir-filters" class="component-filters clearfix">
        <div id="<?php bp_nouveau_filter_container_id(); ?>" class="last filter">
            <div class="select-wrap">
                <select id="event_filter" data-bp-filter="<?php bp_nouveau_filter_component(); ?>">

    <?php
    $events = tribe_get_events(
            apply_filters(
                    'tribe_events_list_widget_query_args', array(
        'eventDisplay' => 'list',
        'posts_per_page' => 10,
        'is_tribe_widget' => true,
        'tribe_render_context' => 'widget',
        'featured' => empty($instance['featured_events_only']) ? false : (bool) $instance['featured_events_only'],
                    )
            )
    );
    ?>
                    <option value="">Select Event</option>
                    <?php
                    foreach ($events as $key => $value) {
                        ?>
                        <option value="<?php echo $value->ID; ?>"><?php echo $value->post_title; ?></option>
                    <?php } ?>

                </select>
                <span class="select-arrow" aria-hidden="true"></span>
            </div>
        </div>
    </div>
    <script>
        jQuery(document).ready(function () {
            jQuery('#members-order-by').val("alphabetical");
            jQuery('#members-all,#members-personal').on('click', function () {
                jQuery('#event_filter').val("");
            });
            jQuery('#members-order-by').on('change', function () {
                jQuery('#event_filter').val("");
            });
        });
    </script>
    <?php
}

/*
 * Filter by events on members list page 
 *  */
add_action('bp_members_directory_member_sub_types', 'members_event_dropdown');

// define the bp_after_parse_args callback 
function filter_members_going_to_events($retval) {
    $pid = isset($_REQUEST['filter']) ? $_REQUEST['filter'] : NULL;
    if (isset($pid) && is_numeric($pid)) {
        global $wpdb;
        $usermetatable = $wpdb->prefix . "usermeta";
        $usereventtable = $wpdb->prefix . "user_assisting_events";
//        $query = "SELECT user_id FROM  0a8eLi_usermeta where meta_key = 'attending_event' and meta_value is not null and meta_value = $pid group by user_id ";
        $query = "SELECT `$usermetatable`.`user_id` FROM `$usermetatable` "
                . "JOIN `$usereventtable` ON `$usermetatable`.`meta_value` = `$usereventtable`.`event_id` "
                . "AND `$usermetatable`.`user_id` = `$usereventtable`.`user_id` "
                . "WHERE `$usermetatable`.`meta_key` = 'attending_event' AND `$usermetatable`.`meta_value` = $pid "
                . "AND `$usereventtable`.`assisting_event` = 1 GROUP BY `$usermetatable`.`user_id` ";
        $custom_ids = $wpdb->get_col($query);
        $custom_ids_str = implode(",", $custom_ids);
        if (!empty($custom_ids_str)) {
            $retval['include'] = $custom_ids_str;
        } else {
            $retval['include'] = 0;
        }
    }
    if(!isset($retval['action'])) $retval['type'] = 'alphabetical';
    return $retval;
}

// add the filter for members search by events
add_filter('bp_after_has_members_parse_args', 'filter_members_going_to_events');

// define the bp_core_get_active_member_count callback 
function filter_bp_core_get_active_member_count($count) {
    global $wpdb;
    $count = (int) $wpdb->get_var("SELECT COUNT(ID) FROM $wpdb->users");
    return $count;
}

// add the filter 
add_filter('bp_core_get_active_member_count', 'filter_bp_core_get_active_member_count', 10, 1);






// Register and load the widget
function wpb_load_widget() {
    register_widget('wpb_widget');
}

add_action('widgets_init', 'wpb_load_widget');

// Creating the widget 
class wpb_widget extends WP_Widget {

    function __construct() {
        parent::__construct(
// Base ID of your widget
                'wpb_widget',
// Widget name will appear in UI
                __('Attendee event List', 'wpb_widget_domain'),
// Widget description
                array('description' => __('This widget is for displaying attendee event list', 'wpb_widget_domain'),)
        );
    }

// Creating widget front-end

    public function widget($args, $instance) {

        $title = apply_filters('widget_title', $instance['title']);

// before and after widget arguments are defined by themes
        echo $args['before_widget'];
        if (!empty($title))
            echo $args['before_title'] . $title . $args['after_title'];

// This is where you run the code and display the output
        global $wpdb;
        $usermetatable = $wpdb->prefix . "usermeta";
        $usereventstable = $wpdb->prefix . "user_assisting_events";
        $current_user = wp_get_current_user();

        $uid = bp_displayed_user_id();
        $user_data = get_userdata($uid);
        echo "<b>" . $user_data->data->display_name . "</b> is going to attend these events..<br/><br/>";
        ?>
        <div class="alert alert-success" id="success-alert" style="padding: 5px 15px; display: none;">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            Your preferences have been updated.
        </div>
        <?php
        if ($uid == $current_user->ID) {
            $getUserAttendingEvents = $wpdb->get_results("SELECT * FROM $usermetatable WHERE user_id=$uid AND meta_key = 'attending_event' and meta_value is not null and meta_value != '' ");
        } else {
            $getUserAttendingEvents = $wpdb->get_results("SELECT meta_value FROM $usermetatable JOIN $usereventstable ON $usermetatable.user_id=$usereventstable.user_id and $usermetatable.meta_value=$usereventstable.event_id WHERE assisting_event=1 AND $usermetatable.user_id=$uid AND meta_key = 'attending_event' and meta_value is not null and meta_value != '' GROUP BY meta_value");
        }

        $event_id = [];
        foreach ($getUserAttendingEvents as $key => $value) {
            $event_id[] = $value->meta_value;
        }
        if (count($event_id) > 0) {
            $args = array(
                'eventDisplay' => 'list',
                'post__in' => $event_id);
            $event_detail = tribe_get_events($args);
        }
        if (isset($event_detail) && count($event_detail) > 0) {
            foreach ($event_detail as $key => $value) {
                $checked = false;
                $asssisting_event = $wpdb->get_var("SELECT assisting_event from $usereventstable where user_id=$uid AND event_id = $value->ID ");
                $checked = $asssisting_event == 1 ? 'checked' : '';
                if ($uid == $current_user->ID) echo "<input type='checkbox' $checked class='event_cbx'  data-id='" . $value->ID . "' user-id='" . $uid . "' '>";
                echo "&nbsp&nbsp&nbsp&nbsp <b><a href=" . get_site_url() . "/event/" . $value->post_name . ">$value->post_title</a></b><br/>";
            }
            echo "<br/>";
        }else {
            echo "No events found";
        }

        echo $args['after_widget'];
        ?>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                jQuery("#success-alert").hide();
                jQuery('.event_cbx').change(function () {
                    var eid, show, uid;
                    eid = jQuery(this).attr('data-id');
                    uid = jQuery(this).attr('user-id');
                    show = this.checked ? 1 : 0
                    var data = {action: 'my_action', eid: eid, uid: uid, show: show};
                    jQuery.post(ajaxurl, data, function (response) {
                        jQuery("#success-alert").fadeTo(1000, 500).slideUp(1000, function () {
                            jQuery("#success-alert").slideUp(500);
                        });
                    });
                });
            });
        </script>

        <?php
    }

// Widget Backend 
    public function form($instance) {
        if (isset($instance['title'])) {
            $title = $instance['title'];
        } else {
            $title = __('New title', 'wpb_widget_domain');
        }
// Widget admin form
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
        <?php
    }

// Updating widget replacing old instances with new
    public function update($new_instance, $old_instance) {
        $instance = array();
        $instance['title'] = (!empty($new_instance['title']) ) ? strip_tags($new_instance['title']) : '';
        return $instance;
    }

}

// Class wpb_widget ends here

add_action('wp_ajax_my_action', 'user_assisting_events');

function user_assisting_events() {
    global $wpdb;
    $event_id = $_POST['eid'];
    $is_show = $_POST['show'];
    $user_id = $_POST['uid'];
    $table = $wpdb->prefix . "user_assisting_events";

    $result = $wpdb->get_results("SELECT * from $table where user_id = $user_id and event_id = $event_id ");
    if (count($result) == 0) {
        $re = $wpdb->insert($table, array('user_id' => $user_id, 'event_id' => $event_id, 'assisting_event' => $is_show));
    } else {
        $re = $wpdb->update($table, array('assisting_event' => $is_show), array('user_id' => $user_id,
            'event_id' => $event_id));
    }
    return wp_send_json_success(array('message' => "Your preferences have been updated"));
    die(); // this is required to return a proper result
}


//     define the bp_xprofile_settings_after_save callback 
function action_bp_xprofile_settings_before_save() {
    global $wpdb;
    $companiesTable = $wpdb->prefix."companies";
    $user_data = wp_get_current_user();
    $emailFieldId = BP_XProfile_Field::get_id_from_name('Email');
    $fieldIds = $_REQUEST['field_ids'];
    $fieldIds = explode(",", $fieldIds);
    
    if (isset($emailFieldId) && in_array($emailFieldId,$fieldIds)) {
        $email_id = $_REQUEST['field_' . $emailFieldId];
        if ($email_id != $user_data->user_email) {
            $res = get_user_by('email', $email_id);
            if ($res) {
                bp_core_add_message(__('That email address is already taken.No changes were made to your account.', 'buddypress'), 'error');
                bp_core_redirect(trailingslashit(bp_displayed_user_domain() . bp_get_profile_slug() . '/edit/group/' . bp_action_variable(1)));
            }
        }
    }
}

// add the action 
add_action('xprofile_data_before_save', 'action_bp_xprofile_settings_before_save', 10, 3);


// define the xprofile_updated_profile callback 
//function action_xprofile_updated_profile( $bp_xprofile_updated_profile_activity, $int, $int1 ) { 
//        global $wpdb;
//        $user_data = get_userdata($bp_xprofile_updated_profile_activity);
//        print_r($user_data);exit;
//        if(trim($user_data->user_login) != trim($user_data->user_email)){
//        $query = $wpdb->query($wpdb->prepare("UPDATE $wpdb->users SET user_login = %s WHERE ID =  $user_data->ID ", $user_data->user_email));        
//        }
//    }; 
//             
//// add the action 
//add_action( 'xprofile_updated_profile', 'action_xprofile_updated_profile', 10, 3 );

//function action_xprofile_updated_profile() 
//{
//global $wpdb;
//    error_log("\n"."in-here", 3, 'userlog.log');
//    $emailFieldId = BP_XProfile_Field::get_id_from_name('Email');
//    if (isset($emailFieldId)) {
//        $email_id = $_REQUEST['field_' . $emailFieldId];
//        $user_data = wp_get_current_user();
//        if ($email_id != $user_data->user_email) {
//            $res = get_user_by('email', $email_id);
//            if ($res) {
//                error_log("\n"."2nd", 3, 'userlog.log');
//                bp_core_add_message(__('That email address is already taken.No changes were made to your account.', 'buddypress'), 'error');
//                bp_core_redirect(trailingslashit(bp_displayed_user_domain() . bp_get_profile_slug() . '/edit/group/' . bp_action_variable(1)));
//            }
//        }
//    }	
//}
//add_action('xprofile_updated_profile', 'action_xprofile_updated_profile', 10, 3);

?>

