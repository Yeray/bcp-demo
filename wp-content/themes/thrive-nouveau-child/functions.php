<?php 
if ( ! function_exists('thrive_user_navigation') ):
function thrive_user_navigation() {
	?>
	<a href="#" class="dropdown-toggle user-notifications-action-name" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
  		<?php $current_user = wp_get_current_user(); ?>
		<?php echo get_avatar( $current_user->ID, 32 ); ?>  &nbsp;
		<span id="navbar-user-dropdown">
			<?php echo esc_html( $current_user->display_name ); ?>
		</span>
		<span class="fa fa-angle-down"></span> 
	</a>
		<?php 
			$user_nav_menu_args = array(
                'menu'              => 'primary',
                'theme_location'    => 'topbarmenu',
                'container'         => 'ul',
                'container_class'   => 'thrive-navigation-class',
                'container_id'      => 'thrive-user-navigation-dropdown',
                'menu_class'        => 'dropdown-menu thrive-navbar-main-menu',
                'echo'				=> false,
                'fallback_cb'		=> false
               );
  			$user_nav_menu = wp_nav_menu( $user_nav_menu_args );
        ?>
    <?php if ( ! empty( $user_nav_menu ) ) { ?>
    	<?php echo thrive_sanity_check( $user_nav_menu ); ?>
    <?php } else { ?>   
	    <ul class="dropdown-menu">
	    	<li>
	    		<a href="<?php echo esc_url(admin_url('nav-menus.php?action=locations')); ?>" id="create-menu-btn" title="<?php esc_attr_e( 'Add \'Top Right Bar\' Menu', 'thrive-nouveau' ); ?>">
	    			<i class="material-icons">add_circle_outline</i>
	    			<?php esc_html_e( 'Add \'Top Right Bar\' Menu', 'thrive-nouveau' ); ?>
	    		</a>
	    	</li>
	    	<li role="separator" class="divider"></li>
	    	<li>
	    		<a href="<?php echo esc_url( wp_logout_url() ); ?>" title="<?php esc_attr_e('Logout', 'thrive-nouveau'); ?>">
	    			<i class="material-icons" style="font-size: 16px;">exit_to_app</i>
	    			<?php esc_html_e('Sign Out', 'thrive-nouveau'); ?>
	    		</a>
	    	</li>
	  	</ul>
    <?php } ?>
	<?php
	return;
}
endif;