<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package thrive
 */
?>
</div><!--.row-->
<?php if (!is_page_template('page-templates/canvas.php')) { ?>
    </div><!-- #content -->
<?php } ?>
</div><!--.row-->
</div><!--#page-container">-->
</div><!--#page-row-->

<div class="row">

    <div id="site-footer-section" class="<?php echo apply_filters('thrive-document-wrapper', 'thrive-document-wrapper'); ?>">



        <footer id="thrive_footer" class="site-footer" role="contentinfo">

            <div class="site-info">

                <div class="container-fluid">

                    <div class="row">

                        <div id="thrive_footer_copytext" class="col-xs-12">

                            <?php
                            $thrive_allowed_html_tags = array(
                                'a' => array(
                                    'href' => array(),
                                    'title' => array()
                                ),
                                'br' => array(),
                                'em' => array(),
                                'strong' => array(),
                            );
                            ?>

                            <?php $thrive_default_copytext = __('&copy; Pocket Games connects 2018. All Rights Reserved.', 'thrive-nouveau'); ?>

                            <?php $thrive_copytext = get_theme_mod('thrive_customizer_footer_copyright_text', $thrive_default_copytext); ?>

                            <?php if (!empty($thrive_copytext)) { ?>

                                <?php echo wp_kses($thrive_copytext, $thrive_allowed_html_tags); ?>

                            <?php } else { ?>

                                <?php echo wp_kses($thrive_default_copytext, $thrive_allowed_html_tags); ?>

                            <?php } ?>

                        </div> <!--.col-xs-12-->
                    </div> <!--.row-->
                </div><!--.container-fluid-->

            </div><!-- .site-info -->

        </footer><!-- #thrive_footer-->
    </div><!--#site-footer-section-->
</div><!--.row-->
<div class="modal fade" id="modalRegisterForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">REQUEST A NEW MEETING </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body mx-3">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-2">
                            <label class="control-label" for="date">Events</label>
                        </div>
                        <div class="col-md-10">
                            <select class="form-control" style="margin-bottom:0;">
                                <option value="">Select Event</option>
                                    <?php
                                    if (function_exists('getUserAttendingEvents')) 
                                            $event_detail = getUserAttendingEvents();
                                    if (isset($event_detail) && count($event_detail) > 0) {
                                        foreach ($event_detail as $key => $value) {
                                            ?>
                                        <option value=<?php echo $value->ID; ?>><?php echo $value->post_title; ?></option>
                                    <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-2">
                            <label class="control-label" for="date">Date</label>
                        </div>
                        <div class="col-md-10">
                            <input class="form-control" id="date" name="date" placeholder="MM/DD/YYYY" type="date"/>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-2">
                            <label class="control-label" for="date">From</label>
                        </div>
                        <div class="col-md-10">
                            <input id="timepicker1" type="text" name="timepicker1"/>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-2">
                            <label class="control-label" for="date">To</label>
                        </div>
                        <div class="col-md-10">
                            <input id="timepicker2" type="text" name="timepicker1"/>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-2">
                            <label class="control-label" for="venue">Venue</label>
                        </div>
                        <div class="col-md-10">
                            <input class="form-control" type="text" style="margin-bottom:0;"/>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-2">
                            <label class="control-label" for="message">Message</label>
                        </div>
                        <div class="col-md-10">
                            <input class="form-control" type="text" style="margin-bottom:0;"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer d-flex justify-content-center">
                <a class="tribe-events-button cancel-meet " href="#" title="cancel">Cancel</a>
                <a class="tribe-events-button request-meet " href="#" title="cancel">Send Request</a>
                <!--<button class="btn btn-deep-orange">Send Request</button>-->
            </div>
        </div>
    </div>
</div>
</div><!--.site-content -(header.php)-->
</div><!-- #page-container-->
<?php do_action('thrive_after_body'); ?>
</div><!-- #page -->

<div id="thrive-scroll-to-top">
    <a href="#" title="<?php esc_html_e('Scroll to top', 'thrive-nouveau'); ?>">
        <span class="sr-only">
<?php esc_html_e('Scroll to top', 'thrive-nouveau'); ?>
        </span>
        <i class="material-icons md-24">vertical_align_top</i>
    </a>
</div>

</div><!--#thrive-global-wrapper-->
<?php wp_footer(); ?>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri() . "/custom.js"; ?>"></script>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri() . "/timepicki.js"; ?>"></script>
</body>
</html>
