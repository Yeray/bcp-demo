jQuery(document).ready(function () {
    console.log("navigator::", navigator);
    jQuery('#timepicker1').timepicki();
    jQuery('#timepicker2').timepicki();

    jQuery('.myevents').on('click', function () {
        jQuery('.tribe-events-loop').css('opacity', '');
    });

    jQuery("#success-alert").hide();
    jQuery('.event_cbx').change(function () {
        var eid, show, uid;
        eid = jQuery(this).attr('data-id');
        uid = jQuery(this).attr('user-id');
        show = this.checked ? 1 : 0
        var data = {action: 'my_action', eid: eid, uid: uid, show: show};
        jQuery.post(ajaxurl, data, function (response) {
            jQuery("#success-alert").fadeTo(1000, 500).slideUp(1000, function () {
                jQuery("#success-alert").slideUp(500);
            });
        });
    });

    jQuery('#members-order-by').val("alphabetical");
    jQuery('#members-all,#members-personal').on('click', function () {
        jQuery('#event_filter').val("");
    });
    jQuery('#members-order-by').on('change', function () {
        jQuery('#event_filter').val("");
    });

    jQuery('#pass1,#pass2').on('keyup', function () {
        jQuery('.pwd-error').remove();
    });
    jQuery('#profile-cancel-submit').on('click', function () {
        jQuery('.pwd-error').remove();
        jQuery('#pass1').val('');
        jQuery('#pass2').val('');
    });
    jQuery('#change-password-button').on('click', function (e) {
        var pass1 = jQuery('#pass1').val();
        var pass2 = jQuery('#pass2').val();
        if (pass1 == "" && pass2 == "") {
            if (jQuery(".pwd-error").length == 0) {
                jQuery("<p class='pwd-error' style='color:red;'>Password is required</p>").insertAfter("#pass1");
            }
            return false;
        }
        if (pass1 != "" && pass1.length < 6) {
            if (jQuery(".pwd-error").length == 0) {
                jQuery("<p class='pwd-error' style='color:red;'>Atleast 6 characters required</p>").insertAfter("#pass1");
            }
            return false;
        }
        if (pass1 != "" && pass2 == "") {
            if (jQuery(".pwd-error").length == 0) {
                jQuery("<p class='pwd-error' style='color:red;'>Please confirm your password</p>").insertAfter("#pass2");
            }
            return false;
        }
        if (pass1 == "" && pass2 != "") {
            if (jQuery(".pwd-error").length == 0) {
                jQuery("<p class='pwd-error' style='color:red;'>Please enter your password</p>").insertAfter("#pass1");
            }
            return false;
        }
        if (pass1 != pass2) {
            if (jQuery(".pwd-error").length == 0) {
                jQuery("<p class='pwd-error' style='color:red;'>Both password should be same</p>").insertAfter("#pass2");
            }
            return false;
        }
        if (pass1 != "" && pass2 != "" && pass1 == pass2) {
            jQuery('.pwd-error').remove();
            jQuery('#submit-loader').addClass('fa fa-spinner fa-spin');
            var data = {action: 'password_change_action', pwd: pass1};
            jQuery.post(ajaxurl, data, function (response) {
                if (response.success == true) {
                    var message = '<div class="alert alert-info password-success-alert alert-dismissible fade in">\n\
                                   <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>\n\
                                   <strong class="p-success-alert"></strong></div>'
                    jQuery('#settings-form').prepend(message);
                    jQuery('#submit-loader').removeClass('fa fa-spinner fa-spin');
                    jQuery('.p-success-alert').html(response.data.message);
                    jQuery(".password-success-alert").fadeTo(1000, 500).slideUp(1000, function () {
                        jQuery(".password-success-alert").slideUp(500);
                        jQuery(".password-success-alert").remove();
                    });
                    jQuery('#pass1').val('');
                    jQuery('#pass2').val('');
                }
            });
        }
    });

    /*  jQuery('.not-availability-events-list').on('change',function(){
     var event_id = jQuery('.not-availability-events-list').val();
     var data = {action: 'check_blocks', eventId: event_id};
     jQuery.post(ajaxurl, data, function (response) {
     if (response.data == 0) {
     var aa =jQuery('#not-availability-blocks1').clone();
     jQuery('#not-availability-blocks2').append(aa);
     jQuery('#not-availability-blocks2').show();
     }
     });
     });
     jQuery('.save-not-availability-block').on('click',function(){
     
     }); */
});


