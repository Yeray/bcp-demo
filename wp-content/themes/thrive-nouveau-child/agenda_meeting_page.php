<?php
/*
  Template Name: dashboard_demo layout
  Template Post Type: post, page, event.
 * @package thrive
 */

get_header();
getMeetings();
$getConfirmedMeetingList = getUserAgendaData('CONFIRMED');
$getIncomingMeetingRequestList = getUserAgendaData('RECEIVED');
$getPendingMeetingRequestList = getUserAgendaData('PENDING');
$getCancelledMeetingList = getUserAgendaData('CANCELLED');
?>

<div id="document-wrapper" class="<?php echo apply_filters('thrive-document-wrapper', 'thrive-document-wrapper'); ?>">
    <div id="sidebar-wrap">
        <div id="sidebar-wrapper">
            <div id="page-sidenav" class="<?php echo thrive_layout_class('sidenav'); ?>">
                <div id="page-sidenav-section">
                    <?php get_template_part('template-parts/sidebar', 'content'); ?>
                </div>
            </div>
        </div>
    </div>
    <div id="page-content-wrapper" >
        <div class="content-sidebar">
            <div class="row">
                <div class="col-md-12">
                    <div id="primary" class="content-area thrive-page-document">
                        <article id="post-0" class="post-0 page type-page status-draft hentry">
                            <div class="entry-content">
                                <div class="article-body">
                                    <div id="tribe-events" class="" data-live_ajax="1" data-datepicker_format="" data-category="" data-featured="">
                                        <div class="tribe-events-before-html"></div>
                                        <span class="tribe-events-ajax-loading"><img class="tribe-events-spinner-medium" src="http://pnc.local/wp-content/plugins/the-events-calendar/src/resources/images/tribe-loading.gif" alt="Loading Events" /></span>
                                        <div id="tribe-events-content-wrapper" class="tribe-clearfix"><input id="tribe-events-list-hash" type="hidden" value="" />
                                            <div class="meeting-tabs agenda-meetings-q">
                                                <ul class="nav nav-tabs">
                                                    <li class="active"><a data-toggle="tab" href="#home">All meetings</a></li>
                                                    <li><a data-toggle="tab" href="#menu1">Incoming meeting request</a><span><?= count($getIncomingMeetingRequestList) ?></span></li>
                                                    <li><a data-toggle="tab" href="#menu2">Pending meeting request</a><span><?= count($getPendingMeetingRequestList) ?></span></li>
                                                    <li><a data-toggle="tab" href="#menu3">Cancelled meetings</a><span><?= count($getCancelledMeetingList) ?></span></li>
                                                </ul>

                                                <div class="tab-content agenda-meetings">
                                                    <div id="home" class="tab-pane fade in active">
                                                        <div id="tribe-events-bar">
                                                            <h2 class="tribe-events-visuallyhidden">Events Search and Views Navigation</h2>
                                                            <?php // tribe_get_template_part('modules/bar');  ?>
                                                            <?php
                                                            if (function_exists('getUserAttendingEvents'))
                                                                $event_detail = getUserAttendingEvents();
                                                            ?>
                                                            <div class="select-wrap1">
                                                                <select class="not-availability-events-list" data-eventid="CONFIRMED" style="margin-right: 20px;">
                                                                    <option value="all">Find Events</option> 
                                                                    <?php
                                                                    if (isset($event_detail) && count($event_detail) > 0) {
                                                                        foreach ($event_detail as $key => $value) {
                                                                            ?>
                                                                            <option value=<?php echo $value->ID; ?>><?php echo $value->post_title; ?></option>
                                                                        <?php
                                                                        }
                                                                    }
                                                                    ?>
                                                                </select>
                                                                <span class="select-arrow" aria-hidden="true"></span>
                                                            </div>

                                                            <div class="select-wrap1">
                                                                <select class="not-availability-events-list1" data-eventid="CONFIRMED" data-upcomingdropdown="yes">
                                                                    <option value="upcoming">Upcoming Meetings </option> 
                                                                    <option value="all">All Meetings</option>
                                                                </select>
                                                                <span class="select-arrow" aria-hidden="true"></span>
                                                            </div>
                                                        </div>
                                                        <div id="tribe-events-content" class="tribe-events-list">
                                                            <div class="tribe-events-loop CONFIRMED">
                                                                <?php
                                                                if (count($getConfirmedMeetingList) > 0) {
                                                                    foreach ($getConfirmedMeetingList as $key => $value) {
                                                                        $userProfileImage = bp_core_fetch_avatar(array('item_id' => $value->user_id, 'alt' => 'Profile Image', 'class' => "attachment-thumbnail size-thumbnail", 'width' => 150, 'height' => 150, 'email' => $value->email));
                                                                        $date = date('M d', strtotime($value->date));
                                                                        $start_time = date('g:i A', strtotime($value->start_hour));
                                                                        $end_time = date('g:i A', strtotime($value->end_hour));
                                                                        ?>
                                                                        <div class="type-tribe_events tribe-clearfix tribe-events-first" style="padding: 20px">
                                                                            <div class="col-md-2">
                                                                                <div class="tribe-events-event-image event-image-n">
                                                                                    <a tabindex="-1" href="">
        <?= $userProfileImage; ?>
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-10">
                                                                                <div class="event-head">
                                                                                    <h3 class="tribe-events-list-event-title">
        <?= $value->name; ?> <span class="member-speciality"><?= $value->job_title; ?> <?php if (!empty($value->job_title) && !empty($value->company_name)) {
            echo ",";
        } ?> <?= $value->company_name; ?></span>
                                                                                    </h3>
                                                                                    <div class="event-content-head">
                                                                                        <a class="tribe-event-url" title="<?= $value->event_name; ?>" href="" rel="bookmark">
        <?= $value->event_name; ?>
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="tribe-events-event-meta">
                                                                                        <div class="author location">
                                                                                            <div class="tribe-event-schedule-details">
                                                                                                <span class="tribe-event-date-start meeting-room-font"><?= $date; ?> @ <?= $start_time; ?></span> - <span class="tribe-event-time meeting-room-font"><?= $end_time; ?></span>
                                                                                                <span class="meeting-room meeting-room-font" style="margin-left: 20px;"><?= $value->location; ?></span>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="tribe-events-list-event-description tribe-events-content description entry-summary">
                                                                                    <i><?= $value->message; ?></i>
                                                                                </div>
                                                                                <div class="meeting-schedule text-right">
                                                                                    <a class="tribe-events-button cancel-meet " href="#" title="Request A Meeting">Cancel meeting</a>
                                                                                    <a class="tribe-events-button request-meet" href="#" title="Request A Meeting">Reschedule meeting</a>
                                                                                    <a class="tribe-events-button request-meet" href="#" title="Message">Message</a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
    <?php }
}
?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="menu1" class="tab-pane fade">
                                                        <div id="tribe-events-bar">
                                                            <h2 class="tribe-events-visuallyhidden">Events Search and Views Navigation</h2>
                                                                    <?php //tribe_get_template_part('modules/bar'); ?>
                                                            <div class="">
                                                                <select class="not-availability-events-list" id="RECEIVED">
                                                                    <option value="">Select Event </option> 
                                                                    <?php
                                                                    if (isset($event_detail) && count($event_detail) > 0) {
                                                                        foreach ($event_detail as $key => $value) {
                                                                            ?>
                                                                            <option value=<?php echo $value->ID; ?>><?php echo $value->post_title; ?></option>
    <?php
    }
}
?>
                                                                </select>
                                                            </div>

                                                            <div class="">
                                                                <select class="not-availability-events-list" data-eventid="RECEIVED" data-upcomingdropdown="yes">
                                                                    <option value="upcoming">Upcoming Meetings </option> 
                                                                    <option value="all">All Meetings</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div id="tribe-events-content" class="tribe-events-list">
                                                            <div class="tribe-events-loop RECEIVED">
                                                                <?php
                                                                if (count($getIncomingMeetingRequestList) > 0) {
                                                                    foreach ($getIncomingMeetingRequestList as $key => $value) {
                                                                        $userProfileImage = bp_core_fetch_avatar(array('item_id' => $value->user_id, 'alt' => 'Profile Image', 'class' => "attachment-thumbnail size-thumbnail", 'width' => 150, 'height' => 150, 'email' => $value->email));
                                                                        $date = date('M d', strtotime($value->date));
                                                                        $start_time = date('g:i A', strtotime($value->start_hour));
                                                                        $end_time = date('g:i A', strtotime($value->end_hour));
                                                                        ?>
                                                                        <div class="type-tribe_events tribe-clearfix tribe-events-first" style="padding: 20px">
                                                                            <div class="col-md-2">
                                                                                <div class="tribe-events-event-image event-image-n">
                                                                                    <a tabindex="-1" href="">
                                                                                        <?= $userProfileImage; ?>
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-10">
                                                                                <div class="event-head">
                                                                                    <h3 class="tribe-events-list-event-title">
        <?= $value->name; ?> <span class="member-speciality"><?= $value->job_title; ?> <?php if (!empty($value->job_title) && !empty($value->company_name)) {
            echo ",";
        } ?> <?= $value->company_name; ?></span>
                                                                                    </h3>
                                                                                    <div class="event-content-head">
                                                                                        <a class="tribe-event-url" title="<?= $value->event_name; ?>" href="" rel="bookmark">
        <?= $value->event_name; ?>
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="tribe-events-event-meta">
                                                                                        <div class="author location">
                                                                                            <div class="tribe-event-schedule-details">
                                                                                                <span class="tribe-event-date-start meeting-room-font"><?= $date; ?> @ <?= $start_time; ?></span> - <span class="tribe-event-time meeting-room-font"><?= $end_time; ?></span>
                                                                                                <span class="meeting-room meeting-room-font" style="margin-left: 20px;"><?= $value->location; ?></span>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="tribe-events-list-event-description tribe-events-content description entry-summary">
                                                                                    <i><?= $value->message; ?></i>
                                                                                </div>
                                                                                <div class="meeting-schedule text-right">
                                                                                    <a class="tribe-events-button cancel-meet " href="#" title="Reject A Meeting">Reject meeting</a>
                                                                                    <a class="tribe-events-button request-meet" href="#" title="Reschedule A Meeting">Reschedule meeting</a>
                                                                                    <a class="tribe-events-button request-meet" href="#" title="Accept a Meeting">Accept meeting</a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
    <?php }
}
?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="menu2" class="tab-pane fade">
                                                        <div id="tribe-events-bar">
                                                            <h2 class="tribe-events-visuallyhidden">Events Search and Views Navigation</h2>
                                                                    <?php //tribe_get_template_part('modules/bar'); ?>
                                                            <div class="">
                                                                <select class="not-availability-events-list" id="PENDING">
                                                                    <option value="">Select Event </option> 
<?php
if (isset($event_detail) && count($event_detail) > 0) {
    foreach ($event_detail as $key => $value) {
        ?>
                                                                            <option value=<?php echo $value->ID; ?>><?php echo $value->post_title; ?></option>
    <?php
    }
}
?>
                                                                </select>
                                                            </div>

                                                            <div class="">
                                                                <select class="not-availability-events-list" data-eventid="PENDING" data-upcomingdropdown="yes">
                                                                    <option value="upcoming">Upcoming Meetings </option> 
                                                                    <option value="all">All Meetings</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div id="tribe-events-content PENDING" class="tribe-events-list">
                                                            <div class="tribe-events-loop">
                                                                                <?php
                                                                                if (count($getPendingMeetingRequestList) > 0) {
                                                                                    foreach ($getPendingMeetingRequestList as $key => $value) {
                                                                                        $userProfileImage = bp_core_fetch_avatar(array('item_id' => $value->user_id, 'alt' => 'Profile Image', 'class' => "attachment-thumbnail size-thumbnail", 'width' => 150, 'height' => 150, 'email' => $value->email));
                                                                                        $date = date('M d', strtotime($value->date));
                                                                                        $start_time = date('g:i A', strtotime($value->start_hour));
                                                                                        $end_time = date('g:i A', strtotime($value->end_hour));
                                                                                        ?>
                                                                        <div class="type-tribe_events tribe-clearfix tribe-events-first" style="padding: 20px">
                                                                            <div class="col-md-2">
                                                                                <div class="tribe-events-event-image event-image-n">
                                                                                    <a tabindex="-1" href="">
        <?= $userProfileImage; ?>
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-10">
                                                                                <div class="event-head">
                                                                                    <h3 class="tribe-events-list-event-title">
        <?= $value->name; ?> <span class="member-speciality"><?= $value->job_title; ?> <?php if (!empty($value->job_title) && !empty($value->company_name)) {
            echo ",";
        } ?> <?= $value->company_name; ?></span>
                                                                                    </h3>
                                                                                    <div class="event-content-head">
                                                                                        <a class="tribe-event-url" title="<?= $value->event_name; ?>" href="" rel="bookmark">
        <?= $value->event_name; ?>
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="tribe-events-event-meta">
                                                                                        <div class="author location">
                                                                                            <div class="tribe-event-schedule-details">
                                                                                                <span class="tribe-event-date-start meeting-room-font"><?= $date; ?> @ <?= $start_time; ?></span> - <span class="tribe-event-time meeting-room-font"><?= $end_time; ?></span>
                                                                                                <span class="meeting-room meeting-room-font" style="margin-left: 20px;"><?= $value->location; ?></span>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="tribe-events-list-event-description tribe-events-content description entry-summary">
                                                                                    <i><?= $value->message; ?></i>
                                                                                </div>
                                                                                <div class="meeting-schedule text-right">
                                                                                    <a class="tribe-events-button cancel-meet " href="#" title="Request A Meeting">Cancel request</a>
                                                                                    <a class="tribe-events-button request-meet" href="#" title="Request A Meeting">Reschedule meeting</a>
                                                                                    <!--<a class="tribe-events-button request-meet" href="#" title="Message">Message</a>-->
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <?php }
                                                                    }
                                                                    ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="menu3" class="tab-pane fade">
                                                        <div id="tribe-events-bar">
                                                            <h2 class="tribe-events-visuallyhidden">Events Search and Views Navigation</h2>
<?php //tribe_get_template_part('modules/bar');  ?>
                                                            <div class="">
                                                                <select class="not-availability-events-list" id="CANCELLED">
                                                                    <option value="">Select Event </option> 
<?php
if (isset($event_detail) && count($event_detail) > 0) {
    foreach ($event_detail as $key => $value) {
        ?>
                                                                            <option value=<?php echo $value->ID; ?>><?php echo $value->post_title; ?></option>
                                                                    <?php
                                                                    }
                                                                }
                                                                ?>
                                                                </select>

                                                                <select class="not-availability-events-list" data-eventid="CANCELLED" data-upcomingdropdown="yes">
                                                                    <option value="upcoming">Upcoming Meetings </option> 
                                                                    <option value="all">All Meetings</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div id="tribe-events-content" class="tribe-events-list">
                                                            <div class="tribe-events-loop CANCELLED">
<?php
if (count($getCancelledMeetingList) > 0) {
    foreach ($getCancelledMeetingList as $key => $value) {
        $userProfileImage = bp_core_fetch_avatar(array('item_id' => $value->user_id, 'alt' => 'Profile Image', 'class' => "attachment-thumbnail size-thumbnail", 'width' => 150, 'height' => 150, 'email' => $value->email));
        $date = date('M d', strtotime($value->date));
        $start_time = date('g:i A', strtotime($value->start_hour));
        $end_time = date('g:i A', strtotime($value->end_hour));
        ?>
                                                                        <div class="type-tribe_events tribe-clearfix tribe-events-first" style="padding: 20px">
                                                                            <div class="col-md-2">
                                                                                <div class="tribe-events-event-image event-image-n">
                                                                                    <a tabindex="-1" href="">
        <?= $userProfileImage; ?>
                                                                                    </a>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-10">
                                                                                <div class="event-head">
                                                                                    <h3 class="tribe-events-list-event-title">
        <?= $value->name; ?> <span class="member-speciality"><?= $value->job_title; ?> <?php if (!empty($value->job_title) && !empty($value->company_name)) {
            echo ",";
        } ?> <?= $value->company_name; ?></span>
                                                                                    </h3>
                                                                                    <div class="event-content-head">
                                                                                        <a class="tribe-event-url" title="<?= $value->event_name; ?>" href="" rel="bookmark">
        <?= $value->event_name; ?>
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="tribe-events-event-meta">
                                                                                        <div class="author location">
                                                                                            <div class="tribe-event-schedule-details">
                                                                                                <span class="tribe-event-date-start meeting-room-font"><?= $date; ?> @ <?= $start_time; ?></span> - <span class="tribe-event-time meeting-room-font"><?= $end_time; ?></span>
                                                                                                <span class="meeting-room meeting-room-font" style="margin-left: 20px;"><?= $value->location; ?></span>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="tribe-events-list-event-description tribe-events-content description entry-summary">
                                                                                    <i><?= $value->message; ?></i>
                                                                                </div>
                                                                                <div class="meeting-schedule text-right">
                                                                                    <a class="tribe-events-button cancel-meet " href="#" title="Request A Meeting">New request</a>
                                                                                    <!--                                                                            <a class="tribe-events-button request-meet" href="#" title="Request A Meeting">Reschedule meeting</a>
                                                                                                                                                                <a class="tribe-events-button request-meet" href="#" title="Message">Message</a>-->
                                                                                </div>
                                                                            </div>
                                                                        </div>
    <?php }
}
?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="tribe-clear"></div>
                                        </div>
                                        <div class="tribe-events-after-html"></div>
                                    </div>

                                </div>
                                <div class="clearfix"></div>
                            </div>

                        </article>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php get_footer(); ?>
