<?php
/**
 * BP Nouveau Component's  filters template.
 *
 * @since 3.0.0
 * @version 3.0.0
 */
?>


<div id="dir-filters" class="component-filters clearfix">
        <div id="<?php bp_nouveau_filter_container_id(); ?>" class="last filter">
            <div class="select-wrap">
                <select id="event_filter" data-bp-filter="<?php bp_nouveau_filter_component(); ?>">

                    <?php
                    $events = tribe_get_events(
                            apply_filters(
                                    'tribe_events_list_widget_query_args', array(
                        'eventDisplay' => 'list',
                        'posts_per_page' => 10,
                        'is_tribe_widget' => true,
                        'tribe_render_context' => 'widget',
                        'featured' => empty($instance['featured_events_only']) ? false : (bool) $instance['featured_events_only'],
                                    )
                            )
                    );
                    ?>
                    <option value="">All Events</option>
                    <?php
                    foreach ($events as $key => $value) {
                        ?>
                        <option value="<?php echo $value->ID; ?>"><?php echo $value->post_title; ?></option>
                    <?php } ?>

                </select>
                <span class="select-arrow" aria-hidden="true"></span>
            </div>
        </div>
</div>


<div id="dir-filters" class="component-filters clearfix">
	<div id="<?php bp_nouveau_filter_container_id(); ?>" class="last filter">
		<label class="bp-screen-reader-text" for="<?php bp_nouveau_filter_id(); ?>">
			<span ><?php bp_nouveau_filter_label(); ?></span>
		</label>
		<div class="select-wrap">
			<select id="<?php bp_nouveau_filter_id(); ?>" data-bp-filter="<?php bp_nouveau_filter_component(); ?>">

				<?php bp_nouveau_filter_options(); ?>

			</select>
			<span class="select-arrow" aria-hidden="true"></span>
		</div>
	</div>
</div>


