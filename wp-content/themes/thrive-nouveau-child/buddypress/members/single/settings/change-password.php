<?php
/** This action is documented in bp-templates/bp-legacy/buddypress/members/single/settings/profile.php */
?>
<form action="" method="post" class="standard-form" id="settings-form">

    <?php if (!is_super_admin()) : ?>

                                                            <!--<label for="pwd"><?php // _e( 'Current Password <span>(required to update email or change current password)</span>', 'thrive' );            ?></label>-->
                                                            <!--<input type="password" name="pwd" id="pwd" size="16" value="" class="hidden settings-input small" <?php // bp_form_field_attributes('password');       ?>/> &nbsp;<a href="<?php // echo wp_lostpassword_url();            ?>" title="<?php // esc_attr_e( 'Password Lost and Found', 'thrive' );            ?>"><?php // _e( 'Lost your password?', 'thrive' );            ?></a>-->

    <?php endif; ?>

        <!--<label for="email"><?php // _e( 'Account Email', 'thrive' );            ?></label>-->

    <!--<input type="email" name="email" id="email" value="<?php // echo bp_get_displayed_user_email();       ?>" class="hidden settings-input" <?php // bp_form_field_attributes('email');       ?>/>-->
    <label for="pass1" class="change-title"><?php _e('Change Your Password', 'thrive'); ?></label>
    <div class="row">
        <div class="col-md-4">
            <?php _e('New Password', 'thrive'); ?>
        </div>   
        <div class="col-md-8">
            <input type="password" name="pass1" id="pass1" size="16" value="" class="settings-input small password-entry" <?php bp_form_field_attributes('password'); ?>/>
            <div id="pass-strength-result"></div>
        </div>
        <div class="col-md-4">
            <?php _e('Repeat New Password', 'thrive'); ?> 
        </div>
        <div class="col-md-8">
            <input type="password" name="pass2" id="pass2" size="16" value="" class="settings-input small password-entry-confirm" <?php bp_form_field_attributes('password'); ?>/>
        </div>
    </div>
    <?php
    /**
     * Fires before the display of the submit button for user general settings saving.
     *
     * @since BuddyPress (1.5.0)
     */
    ?>

    <div class="submit-profile">
        <input type="button" name="profile-group-edit-submit" id="profile-cancel-submit"  value="<?php esc_attr_e('Cancel'); ?> " />
        <button type="button" id="change-password-button" class="auto btn-sbmt">
            <i class="" id="submit-loader"></i> Save Changes
        </button>
    </div>


    <?php // bp_nouveau_submit_button('members-general-settings'); ?>
    <?php
    /**
     * Fires after the display of the submit button for user general settings saving.
     *
     * @since BuddyPress (1.5.0)
     */
    ?>

    <?php wp_nonce_field('bp_settings_general'); ?>

</form>
<?php
/** This action is documented in bp-templates/bp-legacy/buddypress/members/single/settings/profile.php */

?>
