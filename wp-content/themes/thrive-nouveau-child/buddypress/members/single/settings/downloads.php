<?php
/**
 * BuddyPress - Members Settings ( Delete Account )
 *
 * @since 3.0.0
 * @version 3.1.0
 */
bp_nouveau_member_hook('before', 'settings_template');
?>

<h2 class="screen-heading">
    <?php esc_html_e('Download your data', 'buddypress'); ?>
</h2>

<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>


<div class="submit-profile">
    <input type="button" name="button" value="<?php esc_attr_e('Download', 'thrive'); ?>" id="submit" class="auto btn-sbmt" />
</div>

