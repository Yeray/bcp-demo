<?php
/**
 * BuddyPress - Members Settings ( Notifications )
 *
 * @since 3.0.0
 * @version 3.0.0
 */
bp_nouveau_member_hook('before', 'settings_template');
?>

<h2 class="screen-heading email-settings-screen">
    <?php _e('Email Notifications', 'buddypress'); ?>
</h2>

<p class="bp-help-text email-notifications-info">
    <?php _e('Set your email notification preferences.', 'buddypress'); ?>
</p>

<form action="<?php echo esc_url(bp_displayed_user_domain() . bp_get_settings_slug() . '/notifications'); ?>" method="post" class="standard-form" id="settings-form">

    <table class="notification-settings-email">
        <thead>
            <tr>
                <th class="icon">&nbsp;</th>
                <th class="title"><?php _e('Type of notifications', 'buddypress') ?></th>
                <th class="yes"><?php _e('Yes', 'buddypress') ?></th>
                <th class="no"><?php _e('No', 'buddypress') ?></th>
            </tr>
        </thead>

        <tbody>
            <tr id="activity-notification-settings-mentions" class="mention-radio">
                <td>&nbsp;</td>
                <td><?php printf(__('Incoming meeting request', 'buddypress'), bp_core_get_username(bp_displayed_user_id())) ?></td>
                <td class="yes">
                    <input type="radio" name="notifications[notification_activity_new_mention]" id="notification-activity-new-mention-yes" value="yes" />
                    <label for="notification-activity-new-mention-yes" class="bp-screen-reader-text"><?php
                        /* translators: accessibility text */
                        _e('Yes, send email', 'buddypress');
                        ?></label></td>
                <td class="no">
                    <input type="radio" name="notifications[notification_activity_new_mention]" id="notification-activity-new-mention-no" value="no" />
                    <label for="notification-activity-new-mention-no" class="bp-screen-reader-text"><?php
                        /* translators: accessibility text */
                        _e('No, do not send email', 'buddypress');
                        ?>
                    </label>
                </td>
            </tr>

            <tr id="activity-notification-settings-replies" class="mention-radio">
                <td>&nbsp;</td>
                <td><?php _e("Meeting accepted", 'buddypress') ?></td>
                <td class="yes"><input type="radio" name="notifications[notification_activity_new_reply]" id="notification-activity-new-reply-yes" value="yes"  /><label for="notification-activity-new-reply-yes" class="bp-screen-reader-text"><?php
                        /* translators: accessibility text */
                        _e('Yes, send email', 'buddypress');
                        ?></label></td>
                <td class="no"><input type="radio" name="notifications[notification_activity_new_reply]" id="notification-activity-new-reply-no" value="no"   /><label for="notification-activity-new-reply-no" class="bp-screen-reader-text"><?php
                        /* translators: accessibility text */
                        _e('No, do not send email', 'buddypress');
                        ?></label></td>
            </tr>

            <tr id="activity-notification-settings-replies" class="mention-radio">
                <td>&nbsp;</td>
                <td><?php _e("Meeting rejected", 'buddypress') ?></td>
                <td class="yes"><input type="radio" name="notifications[notification_activity_new_reply1]" id="notification-activity-new-reply1-yes" value="yes"  /><label for="notification-activity-new-reply-yes" class="bp-screen-reader-text"><?php
                        /* translators: accessibility text */
                        _e('Yes, send email', 'buddypress');
                        ?></label></td>
                <td class="no"><input type="radio" name="notifications[notification_activity_new_reply1]" id="notification-activity-new-reply1-no" value="no"   /><label for="notification-activity-new-reply-no" class="bp-screen-reader-text"><?php
                        /* translators: accessibility text */
                        _e('No, do not send email', 'buddypress');
                        ?></label></td>
            </tr>

            <tr id="activity-notification-settings-replies" class="mention-radio">
                <td>&nbsp;</td>
                <td><?php _e("Meeting canceled", 'buddypress') ?></td>
                <td class="yes"><input type="radio" name="notifications[notification_activity_new_reply2]" id="notification-activity-new-reply2-yes" value="yes"  /><label for="notification-activity-new-reply-yes" class="bp-screen-reader-text"><?php
                        /* translators: accessibility text */
                        _e('Yes, send email', 'buddypress');
                        ?></label></td>
                <td class="no"><input type="radio" name="notifications[notification_activity_new_reply2]" id="notification-activity-new-reply2-no" value="no"   /><label for="notification-activity-new-reply-no" class="bp-screen-reader-text"><?php
                        /* translators: accessibility text */
                        _e('No, do not send email', 'buddypress');
                        ?></label></td>
            </tr>

            <tr id="activity-notification-settings-replies" class="mention-radio">
                <td>&nbsp;</td>
                <td><?php _e("Meeting reschedule", 'buddypress') ?></td>
                <td class="yes"><input type="radio" name="notifications[notification_activity_new_reply3]" id="notification-activity-new-reply3-yes" value="yes"  /><label for="notification-activity-new-reply-yes" class="bp-screen-reader-text"><?php
                        /* translators: accessibility text */
                        _e('Yes, send email', 'buddypress');
                        ?></label></td>
                <td class="no"><input type="radio" name="notifications[notification_activity_new_reply3]" id="notification-activity-new-reply3-no" value="no"   /><label for="notification-activity-new-reply-no" class="bp-screen-reader-text"><?php
                        /* translators: accessibility text */
                        _e('No, do not send email', 'buddypress');
                        ?></label></td>
            </tr>

            <tr id="activity-notification-settings-replies" class="mention-radio">
                <td>&nbsp;</td>
                <td><?php _e("Alerts", 'buddypress') ?></td>
                <td class="yes"><input type="radio" name="notifications[notification_activity_new_reply4]" id="notification-activity-new-reply4-yes" value="yes"  /><label for="notification-activity-new-reply-yes" class="bp-screen-reader-text"><?php
                        /* translators: accessibility text */
                        _e('Yes, send email', 'buddypress');
                        ?></label></td>
                <td class="no"><input type="radio" name="notifications[notification_activity_new_reply4]" id="notification-activity-new-reply4-no" value="no"   /><label for="notification-activity-new-reply-no" class="bp-screen-reader-text"><?php
                        /* translators: accessibility text */
                        _e('No, do not send email', 'buddypress');
                        ?></label></td>
            </tr>

            <?php
            /**
             * Fires inside the closing </tbody> tag for activity screen notification settings.
             *
             * @since 1.2.0
             */
            ?>
        </tbody>
    </table>
    <div class="submit-profile">
        <input type="button" name="profile-group-edit-submit" id="profile-group-edit-submit"  value="<?php esc_attr_e('Cancel'); ?> " />
        <input type="button" name="button" value="<?php esc_attr_e('Save Changes', 'thrive'); ?>" id="submit" class="auto btn-sbmt" />
    </div>
    <?php // bp_nouveau_submit_button('member-notifications-settings'); ?>

</form>

<?php
bp_nouveau_member_hook('after', 'settings_template');
