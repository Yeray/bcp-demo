<?php
/**
 * BuddyPress - Members Loop
 *
 * @since 3.0.0
 * @version 3.0.0
 */
bp_nouveau_before_loop();
?>

<?php if (bp_get_current_member_type()) : ?>
    <p class="current-member-type"><?php bp_current_member_type_message(); ?></p>
<?php endif; ?>

<?php if (bp_has_members(bp_ajax_querystring('members'))) : ?>

    <?php bp_nouveau_pagination('top'); ?>

    <ul id="members-list" class="<?php bp_nouveau_loop_classes(); ?>">

        <?php while (bp_members()) : bp_the_member(); ?>

            <li <?php bp_member_class(array('item-entry')); ?> data-bp-item-id="<?php bp_member_user_id(); ?>" data-bp-item-component="members">
                <div class="list-wrap _request_member">

                    <div class="item-avatar member-img-profile">
                        <a href="<?php echo bp_core_get_user_domain(bp_get_member_user_id())."profile"; ?>"><?php bp_member_avatar(bp_nouveau_avatar_args()); ?></a>
                    </div>

                    <div class="item">

                        <div class="item-block">

                            <h2 class="list-title member-name">
                                <a href="<?php echo bp_core_get_user_domain(bp_get_member_user_id())."profile"; ?>"><?php bp_member_name(); ?></a>
                            </h2>
                            <?php 
                            if (function_exists('getUserAttendingEvents'))
                                    $attendingEvents = getUserAttendingEvents(bp_get_member_user_id());
                            if (isset($attendingEvents) && count($attendingEvents) > 0) :
                             ?>
                            <p class="item-meta last-activity event-name-d" >
                                <?php foreach ($attendingEvents as $key=>$value){ ?>
                                    <?= mb_strimwidth($value->post_title, 0, 25, '...');?><br/>
                                <?php } ?>
                            </p>
                            <?php endif; ?>

                        </div>

                        <?php if (bp_get_member_latest_update() && !bp_nouveau_loop_is_grid()) : ?>
                            <div class="user-update">
                                <p class="update"> <?php bp_member_latest_update(); ?></p>
                            </div>
                        <?php endif; ?>

                    </div><!-- // .item -->
                    <button class="reqst-meet">Request Meeting</button>
                </div>
            </li>

        <?php endwhile; ?>
    </ul>

    <?php bp_nouveau_pagination('bottom'); ?>

    <?php
else :

    bp_nouveau_user_feedback('members-loop-none');

endif;
?>

<?php bp_nouveau_after_loop(); ?>
