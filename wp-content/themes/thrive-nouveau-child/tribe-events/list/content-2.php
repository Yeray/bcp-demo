<?php
/**
 * List View Content Template
 * The content template for the list view. This template is also used for
 * the response that is returned on list view ajax requests.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/list/content.php
 *
 * @package TribeEventsCalendar
 * @version  4.3
 *
 */
if (!defined('ABSPATH')) {
    die('-1');
}
?>

<div id="tribe-events-content" class="tribe-events-list">


    <?php
    /**
     * Fires before any content is printed inside the list view.
     */
    do_action('tribe_events_list_before_the_content');
    ?>



    <!-- Notices -->
    <?php tribe_the_notices() ?>

    <!-- Events Loop -->

    <!--    <div class="filter-dropdown">
            <div class="dropdown show">
                <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Computer Science Technology
                    <span class="tribe-bar-toggle-arrow"></span>
                </a>
    
                <div></div>
            </div>
        </div>-->
    <?php
    if (function_exists('getUserAttendingEvents'))
        $event_detail = getUserAttendingEvents(); ?>
    <div class="dropdown my_availbe">
        <select class="not-availability-events-list">
            <option value="">Select Event </option> 
            <?php
            if (isset($event_detail) && count($event_detail) > 0) {
                foreach ($event_detail as $key => $value) {
                    ?>
                    <option value=<?php echo $value->ID; ?>><?php echo $value->post_title; ?></option>
                <?php }
            }
            ?>
        </select>
    </div>
    <div id="not-availability-blocks1">
        <form class="not-availability-form">
            <div class="row my-availabity">
                <div class="col-md-6">
                    <p>Block 1: You are NOT available at....</p>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-4">
                                <label class="control-label" for="date">Date</label>
                            </div>
                            <div class="col-md-8">
                                <input class="form-control" id="date" name="date" placeholder="MM/DD/YYYY" type="date"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-4">
                                <label class="control-label" for="date">From</label>
                            </div>
                            <div class="col-md-8">
                                <input id="timepicker1" type="text" name="timepicker1"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-4">
                                <label class="control-label" for="date">To</label>
                            </div>
                            <div class="col-md-8">
                                <input id="timepicker2" type="text" name="timepicker1"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group text-right" style="margin-top: 20px;">
                        <a class="tribe-events-button request-meet save-not-availability-block" href="#" title="Request A Meeting">Save Block</a>
                    </div>
                </div>  
            </div>
        </form>
    </div>
    <div id="not-availability-blocks2"></div>
</div><!-- #tribe-events-content -->

