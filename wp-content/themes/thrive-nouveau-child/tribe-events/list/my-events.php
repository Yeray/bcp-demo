<?php
/**
 * List View Loop
 * This file sets up the structure for the list loop
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/list/loop.php
 *
 * @version 4.4
 * @package TribeEventsCalendar
 *
 */
if (!defined('ABSPATH')) {
    die('-1');
}
?>

<div class="filter-dropdown">
    <div class="dropdown show">
        <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Find Events
            <span class="tribe-bar-toggle-arrow"></span>
        </a>

        <div class="dropdown-menu _allevent" aria-labelledby="dropdownMenuLink">
            <div class="event-search tribe-clearfix">
                <div class="event-form">
                    <label class="event-search-bar-label" for="event-search-label">Events From</label>
                    <input class="form-control" id="date" name="date" placeholder="MM/DD/YYYY" type="date"/>
                    <input type="hidden" name="bar-date-day" id="bar-date-day" class="date-no-param">
                </div>
                <div class="search-bar-filter">
                    <label class="event-search-bar-label" for="event-search-bar">Search</label>
                    <input type="text" name="tribe-bar-search" id="tribe-bar-search" placeholder="Keyword">  
                </div>
                <div class="filter-submit">
                    <input class="filter-submit-btn" type="submit" name="filter-submit-bar" value="Find Events">
                </div>
            </div>
        </div>
    </div>
</div>
<?php
if (function_exists('getUserAttendingEvents')) 
    $event_detail = getUserAttendingEvents();
if (isset($event_detail) && count($event_detail) > 0) {
    foreach ($event_detail as $key => $value) {
        ?>
        <div id="post-<?php echo $value->ID ?>" class="type-tribe_events myevents-list post-<?php echo $value->ID ?> tribe-clearfix tribe-events-first">


            <!-- Event Title -->
            <?php do_action('tribe_events_before_the_event_title') ?>
            <h3 class="tribe-events-list-event-title">
                <a class="tribe-event-url" href="<?php echo esc_url(tribe_get_event_link($value->ID)); ?>" title="<?php echo $value->post_title ?>" rel="bookmark">
                    <?php echo $value->post_title ?>    </a>
            </h3>
            <?php do_action('tribe_events_after_the_event_title') ?>

            <!-- Event Meta -->
            <?php do_action('tribe_events_before_the_meta') ?>
            <?php
            $venue_details = tribe_get_venue_details($value->ID);
            $venue_address = tribe_get_address($value->ID);
            $has_venue_address = (!empty($venue_details['address']) ) ? ' location' : '';

// Organizer
            $organizer = tribe_get_organizer($value->ID);
            ?>
            <div class="tribe-events-event-meta">
                <div class="author <?php echo esc_attr($has_venue_address); ?>">

                    <!-- Schedule & Recurrence Details -->
                    <div class="tribe-event-schedule-details">
                        <?php echo tribe_events_event_schedule_details($value->ID) ?>
                    </div>
                </div>
            </div><!-- .tribe-events-event-meta -->

            <!-- Event Cost -->
            <?php if (tribe_get_cost($value->ID)) : ?>
                <div class="tribe-events-event-cost">
                    <span class="ticket-cost"><?php echo tribe_get_cost($value->ID, true); ?></span>
                    <?php
                    /**
                     * Runs after cost is displayed in list style views
                     *
                     * @since 4.5
                     */
                    do_action('tribe_events_inside_cost')
                    ?>
                </div>
            <?php endif; ?>

            <?php do_action('tribe_events_after_the_meta') ?>

            <!-- Event Image -->
            <?php echo tribe_event_featured_image($value->ID, 'medium'); ?>

            <!-- Event Content -->

            <?php do_action('tribe_events_before_the_content'); ?>
            <div class="tribe-events-list-event-description tribe-events-content description entry-summary">
                <?php echo tribe_events_get_the_excerpt($value->ID, wp_kses_allowed_html('post')); ?>
                <a href="<?php echo esc_url(tribe_get_event_link($value->ID)); ?>" class="tribe-events-read-more" rel="bookmark"><?php esc_html_e('Find out more', 'the-events-calendar') ?> &raquo;</a>
            </div><!-- .tribe-events-list-event-description -->
            <?php do_action('tribe_events_after_the_content'); ?>


        </div>


        <?php
    }
}
?>



