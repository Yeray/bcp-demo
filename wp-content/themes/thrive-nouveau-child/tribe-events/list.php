<?php
/**
 * List View Template
 * The wrapper template for a list of events. This includes the Past Events and Upcoming Events views
 * as well as those same views filtered to a specific category.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/list.php
 *
 * @package TribeEventsCalendar
 *
 */
if (!defined('ABSPATH')) {
    die('-1');
}

do_action('tribe_events_before_template');
?>
<div class="meeting-tabs allevents">
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#allevents">All events</a></li>
        <?php if(is_user_logged_in()) : ?>
        <li><a data-toggle="tab" href="#myevents" class="myevents">My events</a></li>
        <li><a data-toggle="tab" href="#myavaibality">My availability</a></li>
        <?php endif; ?>
    </ul>
    
    <div class="tab-content">
        <div id="allevents" class="tab-pane fade in active">

            <?php tribe_get_template_part('modules/bar'); ?>
            <!-- Main Events Content -->
            <?php tribe_get_template_part('list/content'); ?>
        </div>
        <div id="myevents" class="tab-pane fade">
            <?php // tribe_get_template_part('modules/bar_1'); ?>

            <!-- Main Events Content -->
            <?php tribe_get_template_part('list/content-1'); ?>
        </div>
        <div id="myavaibality" class="tab-pane fade">
            <?php // tribe_get_template_part('modules/bar'); ?>

            <!-- Main Events Content -->
            <?php tribe_get_template_part('list/content-2'); ?>
        </div>
    </div>
    <!-- Tribe Bar -->
</div>


<div class="tribe-clear"></div>

<?php
do_action('tribe_events_after_template');
