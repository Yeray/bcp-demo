<?php

Class Add_API_key {

    const CLIENT_ID = '1_4pq2fas7zgu8ggwwowook08wg8gsww484808gw8c0s004s88cw';
    const CLIENT_SECRET = "2cvbld7v9j0g80sw044gks8w8kkowcw8k0ko80c8sso4ko0ggg";
    const GRANT_TYPE = "http://www.pitchandmatch.com/grants/api_key";
    const SITE_URL = "https://www.pitchandmatch.com";

    public function __construct() {
        $this->api_key = get_option('pm_api_key', $default = false);
    }

    function getToken() {
        if (isset($this->api_key) && !empty($this->api_key)) {
            $url = self::SITE_URL . '/oauth/v2/token?grant_type=' . self::GRANT_TYPE . '&client_id=' . self::CLIENT_ID . '&client_secret=' . self::CLIENT_SECRET . '&api_key=' . $this->api_key;
            $pm_api_authentication = wp_remote_get($url);
            $pm_data = wp_remote_retrieve_body($pm_api_authentication);
            return json_decode($pm_data);
        }
    }

    function getEvents() {
        $access_token = $this->getToken();
        
        if(isset($access_token->access_token)) {
            $event_data = wp_remote_get(self::SITE_URL . '/api/v1/events', array(
                'headers' => array('Authorization' => 'Bearer ' . $access_token->access_token,
                    'Content-Type' => 'application/x-www-form-urlencoded;charset=UTF-8')
                    )
            );
            $event_list = wp_remote_retrieve_body($event_data);
            $pmEvents = json_decode($event_list)->events;
            return $pmEvents;
        }
        if(isset($access_token->error)){
            return $access_token; 
        }
    }

    function getAttendees($url) {
        $access_token = $this->getToken();
        if (isset($access_token->access_token)) {
            $attendee_data_api = wp_remote_get($url, array(
                'headers' => array('Authorization' => 'Bearer ' . $access_token->access_token,
                    'Content-Type' => 'application/json')
                    )
            );
            $attendee_list = wp_remote_retrieve_body($attendee_data_api);
            $attendee_data = json_decode($attendee_list);
            return $attendee_data;
        }
    }

    function createWebhook($event_id) {
        $access_token = $this->getToken();
        if (isset($access_token->access_token)) {
            $data['event'] = $event_id;
            $data['trigger'] = "ATTENDEE_REGISTRATION";
            $data['url'] = get_site_url() . "/wp-json/pm/v1/attendee_data";
            $data = json_encode($data);
            $response = wp_remote_post(self::SITE_URL . "/api/v1/webhooks", array(
                'body' => $data,
                'headers' => array('Authorization' => 'Bearer ' . $access_token->access_token,
                    'Content-Type' => 'application/x-www-form-urlencoded;charset=UTF-8'),
            ));
            return json_decode(wp_remote_retrieve_body($response));  
        }
    }
    
    function deleteWebhook($webhook_id) {
        $access_token = $this->getToken();
        if (isset($access_token->access_token)) {
            $response = wp_remote_request(self::SITE_URL . "/api/v1/webhooks/".$webhook_id, array(
                'method'     => 'DELETE',
                'headers' => array('Authorization' => 'Bearer ' . $access_token->access_token,
                'Content-Type' => 'application/x-www-form-urlencoded;charset=UTF-8'),
            ));
            return wp_remote_retrieve_response_code($response);
        }
    }
    
    function getUserMeetingsData(){
        $current_userID = get_current_user_id();
        $emailData = get_userdata($current_userID);
     
        $access_token = $this->getToken();
        if (isset($access_token->access_token)) {
            $user_meeting_data = wp_remote_get(self::SITE_URL . "/api/v1/meetings?attendee_email=".$emailData->user_email."&type_filter=&time_filter=&time_format_filter=&day=&start_hour=&timezone=", array(
                'headers' => array('Authorization' => 'Bearer ' . $access_token->access_token,
                'Content-Type' => 'application/x-www-form-urlencoded;charset=UTF-8')
            ));
           return json_decode(wp_remote_retrieve_body($user_meeting_data));
        }
        if(isset($access_token->error)){
            return $access_token; 
        }
    }

    function add_new_key() {
        require_once(ROOTPATH . 'add-key-form.php');           //write path of display title 
    }

}
