jQuery(document).ready(function () {
    jQuery("#add_btn").click(function (event) {
        event.preventDefault();
        var apiKey = jQuery('#api_key_id').val();
        if (apiKey == "") {
            if (jQuery(".apikey-error").length == 0) {
                jQuery("<p class='apikey-error' style='color:red;'>Please enter API KEY</p>").insertAfter("#api_key_id");
            }
            return false;
        }
        jQuery('#api_key_id').on('keyup', function () {
            jQuery('.apikey-error').remove();
        });
        jQuery.post(ajaxurl, jQuery("#myForm").serialize(), function (response) {
            jQuery("#myForm").prepend('<div class="alert alert-success success-alert-mess">' + response.message + '</div>');
            jQuery(".success-alert-mess").fadeTo(1000, 500).slideUp(1000, function () {
                jQuery(".success-alert-mess").slideUp(500);
                jQuery(".success-alert-mess").remove();
            });
        });
    });
});
(function($){
    $(document).ready(function() {
        $( '.not-availability-events-list,.not-availability-events-list1').change( function() {                    
            
            var template = '';
            
            var event = $(this).val();
            var event_type = $(this).data('eventid');
            var upcoming = $(this).data('upcomingdropdown');
            $.post(
            {
                url: ajaxurl,
                data: 
                {
                    action: 'ws_attendy_filterby_event',
                    event: event,
                    event_type: event_type,
                    upcoming: upcoming,
                },
                success:function(res)
                {
                    $.each(res, function(key,val){
                        
                        
                        template += `<div class="type-tribe_events tribe-clearfix tribe-events-first" style="padding: 20px">
                                <div class="col-md-2">
                                    <div class="tribe-events-event-image event-image-n">
                                        <a tabindex="-1" href="">
                                            `+val.image+`
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="event-head">
                                        <h3 class="tribe-events-list-event-title">
                                            `+val.name+` <span class="member-speciality">`+val.job_title+" "+val.company_name+`</span>
                                        </h3>
                                        <div class="event-content-head">
                                            <a class="tribe-event-url" title="`+val.event_name+`" href="" rel="bookmark">
                                                `+val.event_name+`
                                            </a>
                                        </div>
                                        <div class="tribe-events-event-meta">
                                            <div class="author location">
                                                <div class="tribe-event-schedule-details">
                                                    <span class="tribe-event-date-start meeting-room-font">`+val.date+` @ `+val.start_time+`</span> - <span class="tribe-event-time meeting-room-font">`+val.end_time+`</span>
                                                    <span class="meeting-room meeting-room-font" style="margin-left: 20px;">`+val.location+`</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tribe-events-list-event-description tribe-events-content description entry-summary">
                                        <i>`+val.message+`</i>
                                    </div>
                                    <div class="meeting-schedule text-right">
                                        <a class="tribe-events-button cancel-meet " href="#" title="Request A Meeting">Cancel meeting</a>
                                        <a class="tribe-events-button request-meet" href="#" title="Request A Meeting">Reschedule meeting</a>
                                        <a class="tribe-events-button request-meet" href="#" title="Message">Message</a>
                                    </div>
                                </div>
                            </div>`;
                    });
                    $("."+event_type).html(template);
                }
            });
        });
    });
}(jQuery));