<?php $api_key = get_option('pm_api_key', $default = false); ?>
<div>
    <h2>Pitch and Match API Key</h2>
    <form id="myForm" method="post" class="validate" action="<?php echo 'admin-ajax.php?action=response='; ?>">
        <?php wp_nonce_field('addapikey', 'add_api_key'); ?>

        <input type="hidden" name="action" value="response"> 

        <table class="form-table">
            <tbody>
                <tr class="form-field">
                    <th scope="row">
                        <label for="keyid">API KEY</label>
                    </th>
                    <td>
                        <input name="keyid" id="api_key_id" placeholder="Enter API key" type="text" value="<?php echo isset($api_key) ? $api_key : ""; ?>">
                    </td>
                </tr>
            </tbody>
        </table>
        <button type="submit" id="add_btn" value="Save Changes" name="btnsubmit" class="button button-primary button-large" style="margin-right: 10px;">Save Changes</button>
    </form>
</div>