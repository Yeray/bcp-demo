<?php

class PM_Rest_Server extends WP_REST_Controller {

    //The namespace and version for the REST SERVER
    var $my_namespace = 'pm/v';
    var $my_version = '1';

    public function register_routes() {
        $namespace = $this->my_namespace . $this->my_version;
        $base = 'attendee_data';
        $meetingbase = 'attendee_meeting_data';
        register_rest_route($namespace, '/' . $base, array(
            array(
                'methods' => WP_REST_Server::CREATABLE,
                'callback' => array($this, 'trigger_attendee_registration'),
                'permission_callback' => array($this, 'trigger_attendee_registration_permission')
            )
        ));
        register_rest_route($namespace, '/' . $meetingbase, array(
            array(
                'methods' => WP_REST_Server::CREATABLE,
                'callback' => array($this, 'attendee_scheduled_meetings'),
                'permission_callback' => array($this, 'trigger_attendee_registration_permission')
            )
        ));
    }

    // Register our REST Server
    public function hook_rest_server() {
        add_action('rest_api_init', array($this, 'register_routes'));
    }

    public function trigger_attendee_registration_permission() {
//    if ( ! current_user_can( 'edit_posts' ) ) {
//          return new WP_Error( 'rest_forbidden', esc_html__( 'You do not have permissions to create data.', 'my-text-domain' ), array( 'status' => 401 ) );
//      }
        return true;
    }

    public function trigger_attendee_registration(WP_REST_Request $request) {
        error_log("\n" . "Inside trigger_attendee_registration" . "\n", 3, 'userlog.log');
        global $wpdb;
        $posttable = $wpdb->prefix . "postmeta";
        $posts = $wpdb->prefix . "posts";
        $usertable = $wpdb->prefix . "usermeta";
        $usereventtable = $wpdb->prefix . "user_assisting_events";

        $request_body = $request->get_body();
        $request_data = json_decode($request_body);
        $event_data = $request_data->event_data;
        $attendee_url = $request_data->attendee;
        if (isset($attendee_url) && !empty($attendee_url)) {
            error_log("\n" . "Attendee URL2 ::" . $attendee_url . "\n", 3, 'userlog.log');
            $pm_api = new Add_API_key();
            $attendee_data = $pm_api->getAttendees($attendee_url);

            if (isset($attendee_data->error)) {
                return json_encode($attendee_data);
            }

            if (isset($attendee_data->attendees)) {
                $attendee_data = $attendee_data->attendees;
                $pm_event_id = $event_data->id;
                $wp_event = $wpdb->get_row("SELECT $posttable.post_id,$posts.post_status from $posttable "
                        . " JOIN $posts ON $posttable.post_id = $posts.ID "
                        . " WHERE $posts.post_status = 'publish' "
                        . " AND $posttable.meta_key = 'linked_event' "
                        . " AND $posttable.meta_value = $pm_event_id ORDER BY $posttable.meta_id DESC limit 1 ");
                
                $args = array(
                    'user_login' => $attendee_data->email,
                    'user_email' => $attendee_data->email,
                    'user_pass' => wp_generate_password($length = 10, $special_chars = true, $extra_special_chars = true),
                    'first_name' => trim($attendee_data->name),
                    'last_name' => trim($attendee_data->lastname),
                    'user_nicename' => trim($attendee_data->name) . " " . trim($attendee_data->lastname),
                    'nickname' => trim($attendee_data->name) . " " . trim($attendee_data->lastname)
                );

                $user_data = get_user_by('email', $attendee_data->email);

//                if user exist update the userdata else create a new user
                if ($user_data) {
                    if ($user_data->display_name == trim($attendee_data->name) . " " . trim($attendee_data->lastname)) {
                        $args['ID'] = $user_data->ID;
                        unset($args['user_pass']);
                        wp_update_user($args);
                        if (isset($wp_event)) {
                            $user_meta = $wpdb->get_var("SELECT COUNT(*) as count from $usertable where user_id = $user_data->ID and meta_key = 'attending_event' and meta_value = $wp_event->post_id ");
                            if ($user_meta == 0) {
//                                link events to users
                                add_user_meta($user_data->ID, 'attending_event', $wp_event->post_id);
                                $wpdb->insert($usereventtable, array('user_id' => $user_data->ID, 'event_id' => $wp_event->post_id));
                            }
                        }

//                        save company data
                        $this->storeCompanyData($user_data->ID, $attendee_data->company);

//                        link user data with buddypress profile
                        try {
                            $this->link_user_profile_to_buddypress($user_data->ID, $attendee_data);
                        } catch (Exception $exc) {
                            echo $exc->getTraceAsString();
                            error_log("\n" . $exc->getTraceAsString(), 3, 'userlog.log');
                        }
                        error_log("\n" . $user_data->ID . "-- Email :: " . $user_data->user_email . "--" . " Updated Successfully" . "\n", 3, 'userlog.log');
                        return wp_send_json_success(array('message' => $user_data->ID . " Successfully updated"));
                    } else {
                        $to = 'tester9065@gmail.com';
                        $subject = 'Registration issue at BCP';
                        $body = ' The registration of the attendee ' . trim($attendee_data->name) . " " . trim($attendee_data->lastname) . " " . ($attendee_data->email) . ' of ' . $event_data->name . " was detected .  The system tries to register this attendee as member of the community but the email is already user by another member. Please proceed to do a manual registration of this member.";
                        $headers = array('Content-Type: text/html; charset=UTF-8');
                        wp_mail($to, $subject, $body, $headers);
                        error_log("\n" . "THe Email allreasy exist in WP DB :: Email Admin" . "\n", 3, 'userlog.log');
                        return wp_send_json_error(array('message' => ' Email is already used by another member'));
                    }
                } else {
                    $id = wp_insert_user($args);
                    $user_data = get_userdata($id);
                    if (!is_wp_error($id)) {
                        $emailFieldId = BP_XProfile_Field::get_id_from_name('Email');
                        if(isset($emailFieldId) && !empty($emailFieldId))
                            xprofile_set_field_data($emailFieldId, $id, $attendee_data->email);
                        
                        wp_new_user_notification($id);
                        if (isset($wp_event)) {
                            add_user_meta($id, 'attending_event', $wp_event->post_id);
                            $wpdb->insert($usereventtable, array('user_id' => $id, 'event_id' => $wp_event->post_id));
                        }

//                        save company data
                        $this->storeCompanyData($user_data->ID, $attendee_data->company);

                        try {
                            $this->link_user_profile_to_buddypress($id, $attendee_data);
                        } catch (Exception $exc) {
                            echo $exc->getTraceAsString();
                            error_log("\n" . $exc->getTraceAsString(), 3, 'userlog.log');
                        }
                        error_log("\n" . $user_data->ID . "-- Email :: " . $user_data->user_email . "--" . " Inserted Successfully" . "\n", 3, 'userlog.log');
                        return wp_send_json_success(array('message' => 'Attendee created successfully',
                            "body" => get_userdata($id)));
                    } else {
                        error_log($id . "\n", 3, 'userlog.log');
                        return wp_send_json_error(array('message' => 'unable to create user at this moment', 'id' => $id));
                    }
                }
            } else {
                error_log("\n" . 'Attendee data not found', 3, 'userlog.log');
                return wp_send_json_error(array('message' => "Attendee data not found"));
            }
        } else {
            error_log("\n" . 'Attendee URL not found', 3, 'userlog.log');
            return wp_send_json_error(array('message' => "Attendee URL not found"));
        }

        return false;
    }

    public function link_user_profile_to_buddypress($user_id, $attendee_data) {
        global $wpdb;
        $profileFieldsValue = array("country" => "Country", "phone" => "phone", "job_title" => "Job Title", "company" => "Company");
        $profileFieldsKey = array_flip($profileFieldsValue);
        $profilefieldstable = $wpdb->prefix . "bp_xprofile_fields";

        $mainField = array("job_title" => "Job Title");
        $arrayFields = array("country" => "Country", "company" => "Company");

        $prfilefields = $wpdb->get_results("SELECT id,name from $profilefieldstable");

        if (!empty($prfilefields)) {
            $attandyDataConvertArray = (array) $attendee_data;

            foreach ($prfilefields as $prfilefield) {
                if (in_array($prfilefield->name, $profileFieldsValue)) {

                    if (in_array($prfilefield->name, $mainField)) {
                        xprofile_set_field_data($prfilefield->id, $user_id, $attandyDataConvertArray[$profileFieldsKey[$prfilefield->name]]);
                    }

                    if (in_array($prfilefield->name, $arrayFields)) {
                        $isCompanyExist = xprofile_get_field_data($prfilefield->id, $user_id);
                        if(empty($isCompanyExist)) xprofile_set_field_data($prfilefield->id, $user_id, $attandyDataConvertArray[$profileFieldsKey[$prfilefield->name]]->name);
                    }
                }
                if ($prfilefield->name == "Company Website") {
                    xprofile_set_field_data($prfilefield->id, $user_id, $attandyDataConvertArray['company']->website);
                }
                if ($prfilefield->name == "Name") {
                    xprofile_set_field_data($prfilefield->id, $user_id, $attandyDataConvertArray['name'] . " " . $attandyDataConvertArray['lastname']);
                }
                if ($prfilefield->name == "Company Logo") {
                    if (isset($attandyDataConvertArray['company']->profile_image) && !empty($attandyDataConvertArray['company']->profile_image)) {
                        xprofile_set_field_data($prfilefield->id, $user_id, "https://www.pitchandmatch.com/" . $attandyDataConvertArray['company']->profile_image);
                    }
                }
            }
        }
    }

    public function storeCompanyData($user_id, $companyData) {
        global $wpdb;
        $company_table = $wpdb->prefix . "companies";

        $isUserLinkedToCompany = get_user_meta($user_id, 'linked_companyID', true);
        if (empty($isUserLinkedToCompany)) {
            if (isset($companyData) && !empty($companyData)) {
                $companyId = $wpdb->get_var("SELECT id from $company_table where name LIKE '".trim($companyData->name)."' limit 1");
                if (empty($companyId)) {
                    $id = $wpdb->insert($company_table, array(
                        'company_id' => $companyData->id,
                        'name' => trim($companyData->name),
                        'address' => isset($companyData->address) ? $companyData->address : NULL,
                        'zipcode' => isset($companyData->zipcode) ? $companyData->zipcode : NULL,
                        'city' => isset($companyData->city) ? $companyData->city : NULL,
                        'country' => isset($companyData->country->name) ? $companyData->country->name : NULL,
                        'profile' => isset($companyData->profile) ? $companyData->profile : NULL,
                        'employees' => isset($companyData->employees) ? $companyData->employees : NULL,
                        'website' => isset($companyData->website) ? $companyData->website : NULL,
                        'profile_image' => isset($companyData->profile_image) ? $companyData->profile_image : NULL
                    ));
                    $lastid = $wpdb->insert_id;
                    update_user_meta($user_id, 'linked_companyID', $lastid);
                } else {
                    update_user_meta($user_id, 'linked_companyID', $companyId);
                }
            } else {
                update_user_meta($user_id, 'linked_companyID', NULL);
            }
        }
    }
}
