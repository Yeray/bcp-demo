<?php

/**
 * @package API-key-plugin
 */

/**
 * Plugin Name: API KEY Plugin
 * Description: This is an PitchandMatch API key plugin. It is used for add, update and store PitchandMatch API key in database.
 * Author: Wama Software
 * 
 */

define('ROOTPATH', plugin_dir_path(__FILE__));

class API_key_plugin {

    function api_menu() {
        $addlist = new Add_API_key();

        add_menu_page(
                'PitchandMatch API Key', 'PM API Key', 'manage_options', 'add-key', array($addlist, 'add_new_key'), 'dashicons-admin-network'
        );
    }

    function ajax_load_scripts() {
        wp_enqueue_script('ajax-key-request', plugin_dir_url(__FILE__) . 'js/ajax-key-request.js', array('jquery'));
        wp_enqueue_style('style', plugin_dir_url(__FILE__) . 'css/style.css', array());
        wp_localize_script('ajax-key-request', 'the_ajax_script', array('ajaxurl' => admin_url('admin-ajax.php')));
    }

    function ajax_add_request() {
        if (isset($_POST['keyid'])) {
            $pm_id = $_POST['keyid'];

            global $wpdb;
            $table_name = $wpdb->prefix . "options";

            try {
                update_option('pm_api_key', $pm_id);
                $return = array('message' => '<b>API Key is set successfully</b>');
                wp_send_json($return);
            } catch (customException $e) {
                $return = array('message' => 'There is a problem saving the api key');
            }
            wp_die();
        }
    }

}

$pm_obj = new API_key_plugin();
add_action('admin_menu', array($pm_obj, 'api_menu'));
add_action('init', array($pm_obj, 'ajax_load_scripts'));
add_action('wp_ajax_response', array($pm_obj, 'ajax_add_request'));

require_once(ROOTPATH . 'add-key.php');


/**
 *  activation 
 */
require_once plugin_dir_path(__FILE__) . 'include/api-key-plugin-activate.php';
register_activation_hook(__FILE__, array('API_key_Activate', 'activate'));


/**
 *  deactivation
 */
require_once plugin_dir_path(__FILE__) . 'include/api-key-plugin-deactivate.php';
register_deactivation_hook(__FILE__, array('API_key_Deactivate', 'deactivate'));

require_once 'class-pm-rest-server.php';
require_once 'custom-functions.php';
?>