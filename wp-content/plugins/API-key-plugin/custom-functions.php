<?php

function add_pm_event_dropdown() {
    if (class_exists('Add_API_key')) {
        global $wpdb;
        $pm = new Add_API_key();
        $events = $pm->getEvents();
        $postmetatable = $wpdb->prefix . "postmeta";
        $posttable = $wpdb->prefix . "posts";
        $allready_linked_events = $wpdb->get_results("SELECT meta_value from $postmetatable "
                                                    . " JOIN $posttable ON $postmetatable.post_id = $posttable.ID "
                                                    . " WHERE $posttable.post_status = 'publish' "  
                                                    . " AND meta_key = 'linked_event' "
                                                    . " AND meta_value is not null and meta_value != '' ");

        $l_events = [];
        foreach ($allready_linked_events as $key => $value) {
            $l_events[] = $value->meta_value;
        }
        add_meta_box('pm_event', 'PITCH AND MATCH EVENTS', 'pm_event_dropdown_callback', 'tribe_events', 'advanced', 'default', array('events' => $events, 'l_events' => $l_events, 'pm' => $pm)
        );
    }
}

function pm_event_dropdown_callback($post_data, $metabox) {
    $events = $metabox['args']['events'];
    $l_events = $metabox['args']['l_events'];
    $pm = $metabox['args']['pm'];
    $wp_to_pm_linked_event = get_post_meta($post_data->ID, 'linked_event', true);
    ?>
    <div>
        <label for="pm_events">Events List</label>
        <select name="pm_events" class="tribe-dropdown">
            <option value="">Please select event</option>
            <?php
            if (!isset($events->error)) {
                foreach ($events as $key => $event) {
                    if (!in_array($event->id, $l_events) || $event->id == $wp_to_pm_linked_event) {
                        ?>
                        <option value="<?php echo $event->id; ?>" <?php echo ($event->id == $wp_to_pm_linked_event) ? "selected" : "" ?>><?php echo $event->name; ?></option>
                        <?php
                    }
                }
            }
            ?>
        </select>
        <?php
        if (isset($pm->api_key) && $pm->api_key == "" && !isset($events->error)) {
            echo "<span style='color:red;padding-left: 30px;'>PitchandMatch API key is not set</span>";
        } elseif (isset($events->error)) {
            echo "<span style='color:red;padding-left: 30px;'>PitchandMatch API key is not valid</span>";
        }
        ?>
        <br/>
    </div>
    <?php
}

/**
 * Events dropwdown from PitchandMatch API.
 */
add_action("add_meta_boxes", "add_pm_event_dropdown");

function add_linked_events_meta($post_id) {
    $wp_pm_prev_linked_event = get_post_meta($post_id, 'linked_event', true);
    

    if (isset($_POST['pm_events']) && !empty($_POST['pm_events'])) {
        update_post_meta($post_id, 'linked_event', $_POST['pm_events']);
        $re = new Add_API_key();

        if (!empty($wp_pm_prev_linked_event) && $wp_pm_prev_linked_event != $_POST['pm_events']) {
            $webhook_id = get_post_meta($post_id, 'linked_webhook_id', true);
            if (!empty($webhook_id)) {
                $res = $re->deleteWebhook($webhook_id);
//            if ($res == 200) 
            }
            update_post_meta($post_id, 'linked_webhook_id', NULL);
        }
        $linked_webhook = get_post_meta($post_id, 'linked_webhook_id', true);
        if (empty($linked_webhook)) {
            $res = $re->createWebhook($_POST['pm_events']);
            if (isset($res->webhooks)) {
                $webhook_id = $res->webhooks->id;
                update_post_meta($post_id, 'linked_webhook_id', $webhook_id);
            }
        }
    }
}

/**
 * Link the WP events to PM events. 
 */
add_action('save_post_tribe_events', 'add_linked_events_meta', 10, 4);



/*
 * API class for receiving attendee data from PM
 *  */
//require get_template_directory() . '/class-pm-rest-server.php';
$wp_webhook_api = new PM_Rest_Server();
$wp_webhook_api->hook_rest_server();

// define the bp_after_parse_args callback 
function filter_members_going_to_events($retval) {
    $retval['exclude'] = get_current_user_id();
    $pid = isset($_REQUEST['filter']) ? $_REQUEST['filter'] : NULL;
    if (isset($pid) && is_numeric($pid)) {
        global $wpdb;
        $usermetatable = $wpdb->prefix . "usermeta";
        $usereventtable = $wpdb->prefix . "user_assisting_events";
//        $query = "SELECT user_id FROM  0a8eLi_usermeta where meta_key = 'attending_event' and meta_value is not null and meta_value = $pid group by user_id ";
        $query = "SELECT `$usermetatable`.`user_id` FROM `$usermetatable` "
                . "JOIN `$usereventtable` ON `$usermetatable`.`meta_value` = `$usereventtable`.`event_id` "
                . "AND `$usermetatable`.`user_id` = `$usereventtable`.`user_id` "
                . "WHERE `$usermetatable`.`meta_key` = 'attending_event' AND `$usermetatable`.`meta_value` = $pid "
                . "AND `$usereventtable`.`assisting_event` = 1 GROUP BY `$usermetatable`.`user_id` ";
        $custom_ids = $wpdb->get_col($query);
        $custom_ids_str = implode(",", $custom_ids);
        if (!empty($custom_ids_str)) {
            $retval['include'] = $custom_ids_str;
        } else {
            $retval['include'] = 0;
        }
    }
    if (!isset($retval['action']))
        $retval['type'] = 'alphabetical';
    return $retval;
}

// add the filter for members search by events
add_filter('bp_after_has_members_parse_args', 'filter_members_going_to_events');

// define the bp_core_get_active_member_count callback 
function filter_bp_core_get_active_member_count($count) {
    global $wpdb;
    $loggedinUserId = get_current_user_id();
    $count = (int) $wpdb->get_var("SELECT COUNT(ID) FROM $wpdb->users where ID != $loggedinUserId");
    return $count;
}

// add the filter 
add_filter('bp_core_get_active_member_count', 'filter_bp_core_get_active_member_count', 10, 1);

// Register and load the widget
function wpb_load_widget() {
    register_widget('wpb_widget');
    register_widget('upcoming_events_widget');
    register_widget('upcoming_meetings_widget');
    register_widget('incoming_meetings_requests_widget');
    register_widget('recent_members_widget');
}

add_action('widgets_init', 'wpb_load_widget');

// Creating the widget 
class Wpb_Widget extends WP_Widget {

    function __construct() {
        parent::__construct(
// Base ID of your widget
                'wpb_widget',
// Widget name will appear in UI
                __('Attendee event List', 'wpb_widget_domain'),
// Widget description
                array('description' => __('This widget is for displaying attendee event list', 'wpb_widget_domain'),)
        );
    }

// Creating widget front-end

    public function widget($args, $instance) {

        $title = apply_filters('widget_title', $instance['title']);

// before and after widget arguments are defined by themes
        echo $args['before_widget'];
        if (!empty($title))
            echo $args['before_title'] . $title . $args['after_title'];

// This is where you run the code and display the output
        global $wpdb;
        $usermetatable = $wpdb->prefix . "usermeta";
        $usereventstable = $wpdb->prefix . "user_assisting_events";
        $current_user = wp_get_current_user();

        $uid = bp_displayed_user_id();
        $user_data = get_userdata($uid);
        ?>
        <div class="alert alert-success" id="success-alert" style="padding: 5px 15px; display: none;">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            Your preferences have been updated.
        </div>
        <?php
        if ($uid == $current_user->ID) {
            $getUserAttendingEvents = $wpdb->get_results("SELECT * FROM $usermetatable WHERE user_id=$uid AND meta_key = 'attending_event' and meta_value is not null and meta_value != '' ");
        } else {
            $getUserAttendingEvents = $wpdb->get_results("SELECT meta_value FROM $usermetatable JOIN $usereventstable ON $usermetatable.user_id=$usereventstable.user_id and $usermetatable.meta_value=$usereventstable.event_id WHERE assisting_event=1 AND $usermetatable.user_id=$uid AND meta_key = 'attending_event' and meta_value is not null and meta_value != '' GROUP BY meta_value");
        }

        $event_id = [];
        foreach ($getUserAttendingEvents as $key => $value) {
            $event_id[] = $value->meta_value;
        }
        if (count($event_id) > 0) {
            $args = array(
                'eventDisplay' => 'list',
                'post__in' => $event_id);
            $event_detail = tribe_get_events($args);
        }
        ?>
        <div class="profile-widget">
            <?php
            if (isset($event_detail) && count($event_detail) > 0) {
                foreach ($event_detail as $key => $value) {
                    $event_meta = get_post_meta($value->ID);
                    $checked = false;
                    $asssisting_event = $wpdb->get_var("SELECT assisting_event from $usereventstable where user_id=$uid AND event_id = $value->ID ");
                    $checked = $asssisting_event == 1 ? 'checked' : '';
                    //if ($uid == $current_user->ID)
                       // echo "<input type='checkbox' $checked class='event_cbx'  data-id='" . $value->ID . "' user-id='" . $uid . "' />";
                    ?>
                    <div class="attending-event">
                        <?php
                        if (isset($event_meta['_thumbnail_id']) && !empty($event_meta['_thumbnail_id'])) {
                            $thumbnail_src = wp_get_attachment_image(( $event_meta['_thumbnail_id'][0]), 'thumbnail');
                            if (isset($thumbnail_src) && !empty($thumbnail_src))
                                echo $thumbnail_src;
                        }
                        ?>
                        <div class="tribe-event-duration">
                            <b><a href=" <?= esc_url(tribe_get_event_link($value->ID)) ?> " title="<?= $value->post_title; ?>"><?= mb_strimwidth($value->post_title, 0, isset($thumbnail_src) && !empty($thumbnail_src) ? 23 : 32, '...'); ?></a></b><br/> 
                            <?php echo tribe_events_event_schedule_details($value->ID); ?>
                        </div>
                    </div>
                    <?php
                }
            }else {
                echo "No events found";
            }
            ?>
        </div>
        <?php
    }

// Widget Backend 
    public function form($instance) {
        if (isset($instance['title'])) {
            $title = $instance['title'];
        } else {
            $title = __('New title', 'wpb_widget_domain');
        }
// Widget admin form
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
        <?php
    }

// Updating widget replacing old instances with new
    public function update($new_instance, $old_instance) {
        $instance = array();
        $instance['title'] = (!empty($new_instance['title']) ) ? strip_tags($new_instance['title']) : '';
        return $instance;
    }

}

// Class wpb_widget ends here
// Class upcoming_events_widget
class Upcoming_Events_Widget extends WP_Widget {

    function __construct() {
        parent::__construct(
                // Base ID of your widget
                'upcoming_events_widget',
                // Widget name will appear in UI
                __('Upcoming events New', 'wpb_widget_domain'),
                // Widget description
                array('description' => __('This widget is for displaying upcoming events', 'wpb_widget_domain'),)
        );
    }

    public function widget($args, $instance) {

        $title = apply_filters('widget_title', $instance['title']);

// before and after widget arguments are defined by themes
        echo $args['before_widget'];
        if (!empty($title))
            echo $args['before_title'] . $title . $args['after_title'];

// This is where you run the code and display the output
        ?>

        <?php
        $events = tribe_get_events(
                apply_filters(
                        'tribe_events_list_widget_query_args', array(
            'eventDisplay' => 'list',
            'posts_per_page' => 5,
            'is_tribe_widget' => true,
            'tribe_render_context' => 'widget',
            'featured' => empty($instance['featured_events_only']) ? false : (bool) $instance['featured_events_only'],
                        )
                )
        );
        ?>
        <div class="upcoming-events-dashboard">
            <?php
            if (isset($events) && count($events) > 0) {
                foreach ($events as $key => $value) {
                    $event_meta = get_post_meta($value->ID);
                    ?>

                    <div class="attending-event">
                        <?php
                        if(isset($event_meta['_thumbnail_id']) && !empty($event_meta['_thumbnail_id'])) {
                        $thumbnail_src = wp_get_attachment_image(( $event_meta['_thumbnail_id'][0]), 'thumbnail');
                        echo $thumbnail_src;
                        }
                        ?>
                        <div class="tribe-event-duration">
                            <?php echo "<b><a title='" . $value->post_title . "' href=" . esc_url(tribe_get_event_link($value->ID)) . " > " . mb_strimwidth($value->post_title, 0, 23, '...') . "</a></b><br/>"; ?>
                            <?php echo tribe_events_event_schedule_details($value->ID); ?>
                        </div>
                    </div>
                    <?php
                }
            } else {
                echo 'No Events Found';
            }
            ?>
        </div>
        <?php
        $events_label_plural = tribe_get_event_label_plural();
        ?>
        <p class="upcoming-tribe-events-widget-link">
            <!--<a href="<?php // echo esc_url(tribe_get_events_link()); ?>" rel="bookmark"><i class="material-icons">local_play</i> <?php // printf(esc_html__('View All %s', 'the-events-calendar'), $events_label_plural); ?></a>-->
            <a href="<?php echo esc_url(tribe_get_events_link()); ?>" rel="bookmark"><i class="material-icons-new-activity"></i> View all events</a>
        </p>
        <?php echo $args['after_widget'];
        ?>
        <!--</ol>-->
        <?php
    }

    // Widget Backend 
    public function form($instance) {
        if (isset($instance['title'])) {
            $title = $instance['title'];
        } else {
            $title = __('New title', 'wpb_widget_domain');
        }
// Widget admin form
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
        <?php
    }

// Updating widget replacing old instances with new
    public function update($new_instance, $old_instance) {
        $instance = array();
        $instance['title'] = (!empty($new_instance['title']) ) ? strip_tags($new_instance['title']) : '';
        return $instance;
    }

}

class Upcoming_Meetings_Widget extends WP_Widget {

    function __construct() {
        parent::__construct(
                // Base ID of your widget
                'upcoming_meetings_widget',
                // Widget name will appear in UI
                __('Upcoming Meetings', 'wpb_widget_domain'),
                // Widget description
                array('description' => __('This widget is for displaying upcoming meetings', 'wpb_widget_domain'),)
        );
    }

    public function widget($args, $instance) {

        $title = apply_filters('widget_title', $instance['title']);

// before and after widget arguments are defined by themes
        echo $args['before_widget'];
        if (!empty($title))
            echo $args['before_title'] . $title . $args['after_title'];

// This is where you run the code and display the output
        ?>

        <div class="upcoming-evet">
            <div class="attending-event">
                <img width="150" height="150" src="<?php echo site_url('/wp-content/uploads/revslider/elearning-slider/slider-1-150x150.jpg'); ?>" class="attachment-thumbnail size-thumbnail" alt="">
                <div class="tribe-event-duration">
                    <b>
                        <a href="#">Kate Robert</a>
                    </b>
                    <br/>
                    <span class="tribe-event-date-start">July 1, 8:00 am</span> - <span class="tribe-event-date-end">5:00 pm</span> 
                </div>
            </div>
            <div class="attending-event">
                <img width="150" height="150" src="<?php echo site_url('/wp-content/uploads/revslider/elearning-slider/slider-1-150x150.jpg'); ?>" class="attachment-thumbnail size-thumbnail" alt="">
                <div class="tribe-event-duration">
                    <b>
                        <a href="#">Alexia Peter</a>
                    </b>
                    <br/>
                    <span class="tribe-event-date-start">July 5, 8:00 am</span> - <span class="tribe-event-date-end">5:00 pm</span> 
                </div>
            </div>
            <div class="attending-event">
                <img width="150" height="150" src="<?php echo site_url('/wp-content/uploads/revslider/elearning-slider/slider-1-150x150.jpg'); ?>" class="attachment-thumbnail size-thumbnail" alt="">
                <div class="tribe-event-duration">
                    <b>
                        <a href="#">Mary Curie</a>
                    </b>
                    <br/>
                    <span class="tribe-event-date-start">July 8, 8:00 am</span> - <span class="tribe-event-date-end">5:00 pm</span> 
                </div>
            </div>
        </div>


        <?php
//        echo 'No Upcoming Meeting Found';
        ?>
        <p class="upcoming-tribe-events-widget-link">
            <a href="<?php echo site_url('/agenda_meeting_page'); ?>" rel="bookmark"><i class="material-icons">date_range</i> View my agenda</a>
        </p>
        <?php echo $args['after_widget'];
        ?>
        <!--</ol>-->
        <?php
    }

    // Widget Backend 
    public function form($instance) {
        if (isset($instance['title'])) {
            $title = $instance['title'];
        } else {
            $title = __('New title', 'wpb_widget_domain');
        }
// Widget admin form
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
        <?php
    }

// Updating widget replacing old instances with new
    public function update($new_instance, $old_instance) {
        $instance = array();
        $instance['title'] = (!empty($new_instance['title']) ) ? strip_tags($new_instance['title']) : '';
        return $instance;
    }

}

class Incoming_Meetings_Requests_Widget extends WP_Widget {

    function __construct() {
        parent::__construct(
                // Base ID of your widget
                'incoming_meetings_requests_widget',
                // Widget name will appear in UI
                __('Incoming Meeting Requests', 'wpb_widget_domain'),
                // Widget description
                array('description' => __('This widget is for displaying incoming meeting requests', 'wpb_widget_domain'),)
        );
    }

    public function widget($args, $instance) {

        $title = apply_filters('widget_title', $instance['title']);

// before and after widget arguments are defined by themes
        echo $args['before_widget'];
        if (!empty($title))
            echo $args['before_title'] . $title . $args['after_title'];

// This is where you run the code and display the output
        ?>
        <div class="upcoming-evet">
            <div class="attending-event">
                <img width="150" height="150" src="<?php echo site_url('/wp-content/uploads/revslider/elearning-slider/slider-1-150x150.jpg'); ?>" class="attachment-thumbnail size-thumbnail" alt="">
                <div class="tribe-event-duration">
                    <b>
                        <a href="#">Kate Robert</a>
                    </b>
                    <br/>
                    <span class="tribe-event-date-start">July 1, 8:00 am</span> - <span class="tribe-event-date-end">5:00 pm</span> 
                </div>
            </div>
        </div>

        <?php
//        echo 'No Incoming Meeting Requests';
        ?>
        <p class="upcoming-tribe-events-widget-link">
            <a href="<?php echo site_url('/agenda_meeting_page'); ?>" rel="bookmark"><i class="material-icons">date_range</i> View my agenda</a>
        </p>
        <?php echo $args['after_widget'];
        ?>
        <!--</ol>-->
        <?php
    }

    // Widget Backend 
    public function form($instance) {
        if (isset($instance['title'])) {
            $title = $instance['title'];
        } else {
            $title = __('New title', 'wpb_widget_domain');
        }
// Widget admin form
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
        <?php
    }

// Updating widget replacing old instances with new
    public function update($new_instance, $old_instance) {
        $instance = array();
        $instance['title'] = (!empty($new_instance['title']) ) ? strip_tags($new_instance['title']) : '';
        return $instance;
    }

}

class Recent_Members_Widget extends WP_Widget {

    /**
     * Constructor method.
     *
     * @since 1.5.0
     */
    public function __construct() {
        $name = _x('(BuddyPress) Recent Members', 'widget name', 'buddypress');
        $description = __('Profile photos of recently active members', 'buddypress');
        parent::__construct(false, $name, array(
            'description' => $description,
            'classname' => 'widget_bp_core_recently_active_widget buddypress widget',
            'customize_selective_refresh' => true,
        ));
    }

    /**
     * Display the Recently Active widget.
     *
     * @since 1.0.3
     *
     * @see WP_Widget::widget() for description of parameters.
     *
     * @param array $args     Widget arguments.
     * @param array $instance Widget settings, as saved by the user.
     */
    public function widget($args, $instance) {
        global $members_template;

        // Get widget settings.
        $settings = $this->parse_settings($instance);

        /**
         * Filters the title of the Recently Active widget.
         *
         * @since 1.8.0
         * @since 2.3.0 Added 'instance' and 'id_base' to arguments passed to filter.
         *
         * @param string $title    The widget title.
         * @param array  $settings The settings for the particular instance of the widget.
         * @param string $id_base  Root ID for all widgets of this type.
         */
        $title = apply_filters('widget_title', $settings['title'], $settings, $this->id_base);

        echo $args['before_widget'];
        echo $args['before_title'] . $title . $args['after_title'];

        // Setup args for querying members.
        $members_args = array(
            'user_id' => 0,
            'type' => 'active',
            'per_page' => $settings['max_members'],
            'max' => $settings['max_members'],
            'populate_extras' => true,
            'search_terms' => false,
        );

        // Back up global.
        $old_members_template = $members_template;
        ?>

        <?php if (bp_has_members($members_args)) : ?>

            <div class="upcoming-evet">

                <?php while (bp_members()) : bp_the_member(); ?>


                    <div class="attending-event">
                        <?php echo bp_core_fetch_avatar(array('item_id' => $members_template->member->id, 'alt' => 'Profile Image', 'class' => "attachment-thumbnail size-thumbnail", 'width' => 150, 'height' => 150, 'email' => $members_template->member->user_email)); ?>
                        <div class="tribe-event-duration">
                            <b>
                                <a href="<?php echo bp_core_get_user_domain(bp_get_member_user_id())."profile"; ?>"><?php bp_member_name(); ?></a>
                            </b>
                            <!--<span class="tribe-event-date-start">July 1 @ 8:00 am</span> - <span class="tribe-event-date-end">@ 5:00 pm</span>--> 
                        </div>
                    </div>



                <?php endwhile; ?>
            </div>

        <?php else: ?>

            <div class="widget-error">
                <?php esc_html_e('There are no recently active members', 'buddypress'); ?>
            </div>

        <?php endif; ?>

        <p class="upcoming-tribe-events-widget-link">
            <a href="<?php echo site_url('/members'); ?>" rel="bookmark"><i class="material-icons-new-people"></i> View all members</a>
        </p>

        <?php
        echo $args['after_widget'];

        // Restore the global.
        $members_template = $old_members_template;
    }

    /**
     * Update the Recently Active widget options.
     *
     * @since 1.0.3
     *
     * @param array $new_instance The new instance options.
     * @param array $old_instance The old instance options.
     * @return array $instance The parsed options to be saved.
     */
    public function update($new_instance, $old_instance) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['max_members'] = strip_tags($new_instance['max_members']);

        return $instance;
    }

    /**
     * Output the Recently Active widget options form.
     *
     * @since 1.0.3
     *
     * @param array $instance Widget instance settings.
     * @return void
     */
    public function form($instance) {

        // Get widget settings.
        $settings = $this->parse_settings($instance);
        $title = strip_tags($settings['title']);
        $max_members = strip_tags($settings['max_members']);
        ?>

        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>">
                <?php esc_html_e('Title:', 'buddypress'); ?>
                <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" style="width: 100%" />
            </label>
        </p>

        <p>
            <label for="<?php echo $this->get_field_id('max_members'); ?>">
                <?php esc_html_e('Max members to show:', 'buddypress'); ?>
                <input class="widefat" id="<?php echo $this->get_field_id('max_members'); ?>" name="<?php echo $this->get_field_name('max_members'); ?>" type="text" value="<?php echo esc_attr($max_members); ?>" style="width: 30%" />
            </label>
        </p>

        <?php
    }

    /**
     * Merge the widget settings into defaults array.
     *
     * @since 2.3.0
     *
     *
     * @param array $instance Widget instance settings.
     * @return array
     */
    public function parse_settings($instance = array()) {
        return bp_parse_args($instance, array(
            'title' => __('Recent Members', 'buddypress'),
            'max_members' => 15,
                ), 'recently_active_members_widget_settings');
    }

}

add_action('wp_ajax_my_action', 'user_assisting_events');

function user_assisting_events() {
    global $wpdb;
    $event_id = $_POST['eid'];
    $is_show = $_POST['show'];
    $user_id = $_POST['uid'];
    $table = $wpdb->prefix . "user_assisting_events";

    $result = $wpdb->get_results("SELECT * from $table where user_id = $user_id and event_id = $event_id ");
    if (count($result) == 0) {
        $re = $wpdb->insert($table, array('user_id' => $user_id, 'event_id' => $event_id, 'assisting_event' => $is_show));
    } else {
        $re = $wpdb->update($table, array('assisting_event' => $is_show), array('user_id' => $user_id,
            'event_id' => $event_id));
    }
    return wp_send_json_success(array('message' => "Your preferences have been updated"));
    die(); // this is required to return a proper result
}

//     define the bp_xprofile_settings_after_save callback 
function action_bp_xprofile_settings_before_save() {
    global $wpdb;
    $companiesTable = $wpdb->prefix . "companies";
    $user_data = wp_get_current_user();
    $emailFieldId = BP_XProfile_Field::get_id_from_name('Email');
    $compnayFieldId = BP_XProfile_Field::get_id_from_name('Company');
    $fieldIds = isset($_REQUEST['field_ids']) ? $_REQUEST['field_ids'] : 0 ;
    $fieldIds = explode(",", $fieldIds);

    if (isset($compnayFieldId) && in_array($compnayFieldId, $fieldIds)) {
        $companyName = $_REQUEST['field_' . $compnayFieldId];
        $ifExistCompany = $wpdb->get_var("SELECT id from $companiesTable WHERE name like '" . trim($companyName) . "'  ");
        if (empty($ifExistCompany)) {
            $id = $wpdb->insert($companiesTable, array('name' => trim($companyName),
                                ));
            $lastid = $wpdb->insert_id;
            update_user_meta(get_current_user_id(), 'linked_companyID', $lastid);
        }else{
            update_user_meta(get_current_user_id(), 'linked_companyID', $ifExistCompany); 
        }
    }
    if (isset($emailFieldId) && in_array($emailFieldId, $fieldIds)) {
        $email_id = $_REQUEST['field_' . $emailFieldId];
        if ($email_id != $user_data->user_email) {
            $res = get_user_by('email', $email_id);
            if ($res) {
                bp_core_add_message(__('That email address is already taken.No changes were made to your account.', 'buddypress'), 'error');
                bp_core_redirect(trailingslashit(bp_displayed_user_domain() . bp_get_profile_slug() . '/edit/group/' . bp_action_variable(1)));
            }
        }
    }
}

// add the action 
add_action('xprofile_data_before_save', 'action_bp_xprofile_settings_before_save', 10, 3);


function getUserAttendingEvents($id = NULL) {

    global $wpdb;
    $current_user = isset($id) ? $id : get_current_user_id();
    $usermetatable = $wpdb->prefix . "usermeta";


    $getUserAttendingEvents = $wpdb->get_results("SELECT * FROM $usermetatable WHERE user_id=$current_user AND meta_key = 'attending_event' and meta_value is not null and meta_value != '' ");

    $event_id = [];
    foreach ($getUserAttendingEvents as $key => $value) {
        $event_id[] = $value->meta_value;
    }
    $args = array(
        'eventDisplay' => 'all',
        'post__in' => count($event_id) > 0 ? $event_id : array(0 => 0));
    if (isset($id))
        $args['posts_per_page'] = 5;
    $event_detail = tribe_get_events($args);
//    echo "<pre>";
//    $abc = get_events();
//    print_r($abc);
//    print_r($event_detail);
//    exit();
    return $event_detail;
}

add_action('wp_ajax_password_change_action', 'changeUserPassword');

function changeUserPassword(){
   $password = $_POST['pwd'];
   $id = get_current_user_id();
   $args = array('ID'=>$id,'user_pass' => $password);
   if(!empty($id)) {
     wp_update_user($args);
     return wp_send_json_success(array('message' => "Your password is successfully updated"));
   }else{
       wp_send_json_error(array('message' => "A problem occured while updating the password"));
   }
   
}

/*
 * Buddypress profile add tabs 
 * 
 */
add_action( 'bp_setup_nav', 'bbg_customize_profile_tabs', 999 );
 function bbg_customize_profile_tabs(){
     global $bp;
     
     $parentSlug = "profile";
     bp_core_new_subnav_item( array( 
	'name' => 'Password',
	'slug' => 'password', 
	'parent_url' => $bp->loggedin_user->domain . $parentSlug . "/",
	'parent_slug' => $parentSlug,
	'position' => 20,
	'user_has_access' => bp_displayed_user_id() == get_current_user_id() ? TRUE : FALSE,
	'screen_function' => 'changePassword'
	) 
);
  
  bp_core_new_subnav_item( array( 
	'name' => 'E-mail Notifications',
	'slug' => 'notifications', 
	'parent_url' => $bp->loggedin_user->domain . $parentSlug . "/",
	'parent_slug' => $parentSlug,
	'position' => 999,
        'user_has_access' => bp_displayed_user_id() == get_current_user_id() ? TRUE : FALSE,
	'screen_function' => 'emailNotification'
	) 
); 
  
  bp_core_new_subnav_item( array( 
	'name' => 'Download',
	'slug' => 'download', 
	'parent_url' => $bp->loggedin_user->domain . $parentSlug . "/",
	'parent_slug' => $parentSlug,
	'position' => 999,
        'user_has_access' => bp_displayed_user_id() == get_current_user_id() ? TRUE : FALSE,
	'screen_function' => 'myDownloads'
	) 
); 
  bp_core_new_subnav_item( array( 
	'name' => 'Delete Account',
	'slug' => 'delete', 
	'parent_url' => $bp->loggedin_user->domain . $parentSlug . "/",
	'parent_slug' => $parentSlug,
	'position' => 999,
        'user_has_access' => bp_displayed_user_id() == get_current_user_id() ? TRUE : FALSE,
	'screen_function' => 'deleteAccount'
	) 
); 

 }
 
function changePassword() {
   add_action( 'bp_template_content', 'changepassword_page_show_screen_content' );
   bp_core_load_template( apply_filters( 'bp_core_template_plugin', 'members/single/plugins' ) );
}

function changepassword_page_show_screen_content() {
    bp_get_template_part( 'members/single/settings/change-password' );
}

function deleteAccount() {
   add_action( 'bp_template_content', 'delete_page_show_screen_content' );
   bp_core_load_template( apply_filters( 'bp_core_template_plugin', 'members/single/plugins' ) );
}

function delete_page_show_screen_content() {
    bp_get_template_part( 'members/single/settings/delete-my-account' );
}

function emailNotification() {
   add_action( 'bp_template_content', 'emailnotification_page_show_screen_content' );
   bp_core_load_template( apply_filters( 'bp_core_template_plugin', 'members/single/plugins' ) );
}

function emailnotification_page_show_screen_content() {
    bp_get_template_part( 'members/single/settings/email-notifications');
}

function myDownloads(){
    add_action( 'bp_template_content', 'download_page_show_screen_content' );
   bp_core_load_template( apply_filters( 'bp_core_template_plugin', 'members/single/plugins' ) );
}

function download_page_show_screen_content() {
    bp_get_template_part( 'members/single/settings/downloads');
}


// Blocks pages from users who aren't logged in.
function bp_redirect_pages_if_user_not_looged_in() {
    global $bp;
    $url = $_SERVER['REQUEST_URI'];
    $explode_url = explode("/", $url);
    // Determines whether a user is logged in. If not, user is re-directed to log-in page when trying to access defined BP pages.
    if (!is_user_logged_in() && ( bp_is_members_component() || bp_is_user() || bp_is_user_profile() || in_array('agenda_meeting_page', $explode_url) )) {
        // redirects logged-out users to login page. See https://codex.wordpress.org/Function_Reference/auth_redirect
        bp_core_redirect($bp->root_domain . '/' . 'account-sign-in');
    }
}
add_action( 'template_redirect', 'bp_redirect_pages_if_user_not_looged_in' );


function getMeetings(){
    global $wpdb;
    $posttable = $wpdb->prefix . "postmeta";
    $posts = $wpdb->prefix . "posts";
    $users = $wpdb->prefix . "users";
    $meeting_request = $wpdb->prefix . "attendee_meeting_request";
    $userMeetings = new Add_API_key();
    $meetingData = $userMeetings->getUserMeetingsData();
    $userID = get_current_user_id();

    if (count($meetingData->meetings) > 0) {
        foreach ($meetingData->meetings as $key => $value) {
            $wp_event = $wpdb->get_row("SELECT $posttable.post_id,$posts.post_status from $posttable
                        JOIN $posts ON $posttable.post_id = $posts.ID
                        WHERE $posts.post_status = 'publish'
                        AND $posttable.meta_key = 'linked_event'
                        AND $posttable.meta_value = '" . $value->event . "' ORDER BY $posttable.meta_id DESC limit 1 ");

            $userData = get_user_by('email', $value->email);
            $uid = get_current_user_id();
            $date = str_replace("/", "-", $value->day);
            $dateformat = date('Y-m-d', strtotime($date));

            if ($userData) {
                $userMeetingCount = $wpdb->get_var("SELECT id FROM $meeting_request WHERE user_id='" . $userData->data->ID . "'
                        AND email='" . $value->email . "'
                        AND start_hour='" . $value->start_hour . "'
                        AND end_hour='" . $value->end_hour . "'
                        AND date='" . $dateformat . "'"
                );
                if (empty($userMeetingCount)) {
                    $wpdb->insert($meeting_request, array(
                        'initiator_user_id' => $uid,
                        'user_id' => $userData->data->ID,
                        'name' => $value->name,
                        'email' => $value->email,
                        'event_id' => $wp_event->post_id,
                        'date' => $dateformat,
                        'start_hour' => $value->start_hour,
                        'end_hour' => $value->end_hour,
                        'timezone' => $value->timezone,
                        'location' => $value->location,
                        'message' => $value->message,
                        'type' => $value->type
                    ));
                }
                elseif ($userMeetingCount){
                    $wpdb->update($meeting_request, array(
                        'initiator_user_id' => $uid,
                        'user_id' => $userData->data->ID,
                        'name' => $value->name,
                        'email' => $value->email,
                        'event_id' => $wp_event->post_id,
                        'date' => $dateformat,
                        'start_hour' => $value->start_hour,
                        'end_hour' => $value->end_hour,
                        'timezone' => $value->timezone,
                        'location' => $value->location,
                        'message' => $value->message,
                        'type' => $value->type
                    ), array('id' => $userMeetingCount));
                }
            }
        }
    }
}
//add_action( 'wp_login' , 'getMeetings', 10, 2 );

/*
 * Get users meetings data
 */
function getUserAgendaData($type){
    global $wpdb;
    $posts = $wpdb->prefix . "posts";
    $userID = get_current_user_id();
    $meetingtable = $wpdb->prefix . "attendee_meeting_request";
    $compnayFieldId = BP_XProfile_Field::get_id_from_name('Company');
    $jobTitleId = BP_XProfile_Field::get_id_from_name('Job Title');

    $meetingData = $wpdb->get_results("SELECT * FROM $meetingtable WHERE initiator_user_id = $userID AND type = '$type' ");

    foreach ($meetingData as $key => $value) {
        $meetingData[$key]->event_name = $wpdb->get_var("SELECT post_title from $posts WHERE ID = '" . $value->event_id . "'");
        $meetingData[$key]->company_name = xprofile_get_field_data($compnayFieldId, $value->user_id);
        $meetingData[$key]->job_title = xprofile_get_field_data($jobTitleId, $value->user_id);
    }
    return $meetingData;
}

add_action( 'wp_ajax_ws_attendy_filterby_event', 'ws_attendy_filterby_event' );
function ws_attendy_filterby_event()
{   
    
    global $wpdb;
    $userID = get_current_user_id();
    $table = $wpdb->prefix . "attendee_meeting_request";
    $posts = $wpdb->prefix . "posts";
    $compnayFieldId = BP_XProfile_Field::get_id_from_name('Company');
    $jobTitleId = BP_XProfile_Field::get_id_from_name('Job Title');
    if($_POST['event'] == 'upcoming')
    {
        $evenatList = $wpdb->get_results("SELECT * from $table WHERE date BETWEEN '" . date('Y-m-d') . "' AND '2500-01-01' AND initiator_user_id = $userID AND type='".$_POST['event_type']."'");
    }
    else if($_POST['event'] == 'all')
    {
        $evenatList = $wpdb->get_results("SELECT * from $table WHERE initiator_user_id = $userID AND type='".$_POST['event_type']."'");
    }
    else
    {
        $evenatList = $wpdb->get_results("SELECT * from $table WHERE event_id = '" . $_POST['event'] . "' AND type = '".$_POST['event_type']."' AND initiator_user_id = $userID");
    }
    
    foreach ($evenatList as $key => $value) {
        $evenatList[$key]->image = bp_core_fetch_avatar(array('item_id' => $value->user_id, 'alt' => 'Profile Image', 'class' => "attachment-thumbnail size-thumbnail", 'width' => 150, 'height' => 150, 'email' => $value->email ));
        $evenatList[$key]->date = date('M d',strtotime($value->date));
        $evenatList[$key]->start_time = date('g:i A', strtotime($value->start_hour));
        $evenatList[$key]->end_time = date('g:i A',strtotime($value->end_hour));
        $evenatList[$key]->event_name = $wpdb->get_var("SELECT post_title from $posts WHERE ID = '" . $value->event_id . "'");
        $evenatList[$key]->company_name = xprofile_get_field_data($compnayFieldId, $value->user_id);
        $evenatList[$key]->job_title = xprofile_get_field_data($jobTitleId, $value->user_id);
    }
    return wp_send_json($evenatList);
}

//function getUpcomingMeetings(){
//    global $wpdb;
//    $userID = get_current_user_id();
//    $meetingtable = $wpdb->prefix . "attendee_meeting_request";
//    $posts = $wpdb->prefix . "posts";
//    $compnayFieldId = BP_XProfile_Field::get_id_from_name('Company');
//    $jobTitleId = BP_XProfile_Field::get_id_from_name('Job Title');
//    $date = date("Y-m-d");
//
//    $meetingList =  $wpdb->get_results("SELECT * FROM $meetingtable WHERE initiator_user_id='" . $userID . "' AND date BETWEEN '" . $date . "' AND '2500-01-01'");
//    
//    foreach($meetingList as $key => $value){
//        $meetingList[$key]->image = bp_core_fetch_avatar(array('item_id' => $value->user_id, 'alt' => 'Profile Image', 'class' => "attachment-thumbnail size-thumbnail", 'width' => 150, 'height' => 150, 'email' => $value->email ));
//        $meetingList[$key]->date = date('M d',strtotime($value->date));
//        $meetingList[$key]->start_time = date('g:i A', strtotime($value->start_hour));
//        $meetingList[$key]->end_time = date('g:i A',strtotime($value->end_hour));
//        $meetingList[$key]->event_name = $wpdb->get_var("SELECT post_title from $posts WHERE ID = '" . $value->event_id . "'");
//        $meetingList[$key]->company_name = xprofile_get_field_data($compnayFieldId, $value->user_id);
//        $meetingList[$key]->job_title = xprofile_get_field_data($jobTitleId, $value->user_id);
//    }
////    return wp_send_json($meetingList);
//}
?>