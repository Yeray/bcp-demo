<?php

/**
 * @package API-key-plugin
 */
class API_key_Activate {

    public static function activate() {
        flush_rewrite_rules();

        global $wpdb;
        $companies = $wpdb->prefix . "companies";
        $assistingEvents = $wpdb->prefix . "user_assisting_events";
        $meetings = $wpdb->prefix . "attendee_meeting_request";
        $wpdb->query("CREATE TABLE IF NOT EXISTS $companies (
                        id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
                        company_id INT(11) NOT NULL,
                        name VARCHAR(75) NOT NULL,
                        address TEXT DEFAULT NULL,
                        zipcode VARCHAR(50) DEFAULT NULL,
                        city VARCHAR(50) DEFAULT NULL,
                        country VARCHAR(75) DEFAULT NULL,
                        profile TEXT DEFAULT NULL,
                        employees VARCHAR(55) DEFAULT NULL,
                        website VARCHAR(75) DEFAULT NULL,
                        profile_image TEXT DEFAULT NULL
            )");
        $wpdb->query("CREATE TABLE IF NOT EXISTS $assistingEvents (
                        id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
                        user_id INT(55) NOT NULL,
                        event_id INT(55) NOT NULL,
                        assisting_event INT(11) DEFAULT 1
            )");
        $wpdb->query("CREATE TABLE IF NOT EXISTS $meetings (
                        id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
                        initiator_user_id INT(55) NOT NULL,
                        user_id BIGINT(55) NOT NULL,
                        name VARCHAR(75) NOT NULL,
                        email VARCHAR(75) NOT NULL,
                        event_id INT(55) DEFAULT NULL,
                        date DATE NOT NULL,
                        start_hour TIME NOT NULL,
                        end_hour TIME NOT NULL,
                        timezone VARCHAR(55)  DEFAULT NULL,
                        location VARCHAR(150) NOT NULL,
                        message VARCHAR(150)  DEFAULT NULL,
                        type VARCHAR(20) NOT NULL
            )");
    }

}
